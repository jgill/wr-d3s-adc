#include <errno.h>
#include <stdlib.h>

#include <libmockturtle.h>
#include "libd3s-internal.h"

static struct trtl_proto_header d3s_in_hdr_sync = {
  .seq = 0,
  .flags = TRTL_PROTO_FLAG_SYNC,
  
};

const char *d3s_errors[] = {
	"The selected mode is not correct",
	"Invalid Real-Time application running",
};


/**
 * It returns a string messages corresponding to a given error code. If
 * it is not a libd3s error code, it will run trtl_strerror()
 * @param[in] err error code
 * @return a message error
 */
const char *d3s_strerror(int err)
{
	if (err < ED3S_INVALID_MODE || err >= __ED3S_MAX_ERROR_NUMBER)
		return trtl_strerror(err);

	return d3s_errors[err - ED3S_INVALID_MODE];
}


/**
 * It initializes the D3S library. It must be called before doing
 * anything else. If you are going to load/unload D3S devices, then
 * you have to un-load (d3s_exit()) e reload (d3s_init()) the library.
 *
 * This library is based on the libtrtl, so internally, this function also
 * run trtl_init() in order to initialize the TRTL library.
 * @return 0 on success, otherwise -1 and errno is appropriately set
 */
int d3s_init()
{
	int err;

	err = trtl_init();
	if (err)
		return err;

	return 0;
}





/**
 * It releases the resources allocated by d3s_init(). It must be called when
 * you stop to use this library. Then, you cannot use functions from this
 * library.
 */
void d3s_exit()
{
	trtl_exit();
}


/**
 * It gets the firmware version
 * @param[in] dev device token
 * @param[out] version the RT application version
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int d3s_version(struct d3s_node *dev, struct trtl_rt_version *version)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	int in_slot = 0, out_slot = 0;

	switch (d3s->mode) {
	case D3S_MODE_MASTER:
		in_slot = D3S_IN_CONTROL;
		out_slot = D3S_OUT_CONTROL;
		break;
	case D3S_MODE_SLAVE:
		in_slot = D3S_SLAVE_IN_CONTROL;
		out_slot = D3S_SLAVE_OUT_CONTROL;
		break;
	}
	return trtl_rt_version_get(d3s->trtl, version, in_slot, out_slot);
}


/**
 * It pings the firmware to verify if it is still alive
 * @param[in] dev device token
 * @return 0 when the firmware is still alive, otherwise -1 and errno is set
 *         appropriately
 */
int d3s_ping(struct d3s_node *dev)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	int in_slot = 0, out_slot = 0;

	switch (d3s->mode) {
	case D3S_MODE_MASTER:
		in_slot = D3S_IN_CONTROL;
		out_slot = D3S_OUT_CONTROL;
		break;
	case D3S_MODE_SLAVE:
		in_slot = D3S_SLAVE_IN_CONTROL;
		out_slot = D3S_SLAVE_OUT_CONTROL;
		break;
	}

	return trtl_rt_ping(d3s->trtl, in_slot, out_slot);
}


/**
 * It resets the CPU where the D3S firmware is running
 * @param[in] dev device token
 *
 *
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int d3s_reset(struct d3s_node *dev)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	unsigned int index = d3s->mode;
	int err;

	err = trtl_cpu_disable(d3s->trtl, index);
	if (err)
		return err;
	err = trtl_cpu_stop(d3s->trtl, index);
	if (err)
		return err;

	err = trtl_cpu_enable(d3s->trtl, index);
	if (err)
		return err;

	return trtl_cpu_start(d3s->trtl, index);
}


/**
 * Check if the RT-app running version is compatible with the current
 * library
 * @param[in] dev device token
 * @return 1 if the version is correct, 0 otherwise and errno is
 *         appropriately set
 */
int d3s_version_is_valid(struct d3s_node *dev)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	struct trtl_rt_version version;
	uint32_t rt_id = 0;
	int err;

	errno = 0;
	err = d3s_version(dev, &version);
	if (err)
		return 0;

	switch (d3s->mode) {
	case D3S_MODE_MASTER:
		rt_id = D3S_RT_ID_MASTER;
		break;
	case D3S_MODE_SLAVE:
		rt_id = D3S_RT_ID_SLAVE;
		break;
	}

	if (version.fpga_id != D3S_APP_ID || version.rt_id != rt_id) {
		errno = ED3S_INVALID_IN_APP;
		return 0;
	}

	return 1;
}


/**
 * It opens and initialize the configuration for the given device.
 * @param[in] device_id device identifier
 * @param[in] is_lun 1 if device_id is a LUN
 *
 * The function validate the running firmware before continuing. This implies
 * that the firmware must be programmed before using the mock turtle tools.
 *
 * @return It returns an anonymous d3s_node structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
static struct d3s_node *d3s_open(uint32_t device_id, unsigned int is_lun, enum d3s_mode mode)
{
	struct d3s_desc *d3s;
	int err =0;

	d3s = malloc(sizeof(struct d3s_desc));
	if (!d3s)
		return NULL;

	/* TODO check if the mode is compatible with the given device */
	if (err) {
		errno = ED3S_INVALID_MODE;
		goto out;
	}
	d3s->mode = mode;

	/* Prepare template for messages */
	switch (d3s->mode) {
	case D3S_MODE_MASTER:
		d3s_in_hdr_sync.rt_app_id = D3S_RT_ID_MASTER;
		d3s_in_hdr_sync.slot_io = (D3S_IN_CONTROL << 4) | D3S_OUT_CONTROL;
		break;
	case D3S_MODE_SLAVE:
		d3s_in_hdr_sync.rt_app_id = D3S_RT_ID_SLAVE;
		d3s_in_hdr_sync.slot_io = (D3S_SLAVE_IN_CONTROL << 4)
					| D3S_SLAVE_OUT_CONTROL;
		break;
	}

	if (is_lun)
		d3s->trtl = trtl_open_by_lun(device_id);
	else
		d3s->trtl = trtl_open_by_fmc(device_id);
	if (!d3s->trtl)
		goto out;

	d3s->dev_id = device_id;


	d3s->hmq = trtl_hmq_open(d3s->trtl, (d3s_in_hdr_sync.slot_io >> 4 & 0xF),
				TRTL_HMQ_INCOMING);
	if (!d3s->hmq)
		goto out_open;

	if(!d3s_version_is_valid((struct d3s_node *)d3s))
		goto out_valid;

	return (struct d3s_node *)d3s;

out_valid:
	trtl_hmq_close(d3s->hmq);
out_open:
	trtl_close(d3s->trtl);
out:
	free(d3s);
	return NULL;
}

/**
 * Open a D3S node device using FMC ID
 * @param[in] device_id FMC device identificator
 * @return It returns an anonymous d3s_node structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
struct d3s_node *d3s_open_by_fmc(uint32_t device_id, enum d3s_mode mode)
{
	return d3s_open(device_id, 0, mode);
}


/**
 * Open a D3S node device using LUN
 * @param[in] lun an integer argument to select the device or
 *            negative number to take the first one found.
 * @return It returns an anonymous d3s_node structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
struct d3s_node *d3s_open_by_lun(int lun, enum d3s_mode mode)
{
	return d3s_open(lun, 1, mode);
}


/**
 * It closes a D3S device opened with one of the following function:
 * d3s_open_by_lun(), d3s_open_by_fmc()
 * @param[in] dev device token
 */
void d3s_close(struct d3s_node *dev)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;

	trtl_hmq_close(d3s->hmq);
	trtl_close(d3s->trtl);
	free(d3s);
	dev = NULL;
}


int d3s_stream_start(struct d3s_node *dev, uint32_t streamid)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	struct trtl_msg msg;
	struct trtl_proto_header hdr = d3s_in_hdr_sync;

	hdr.msg_id = D3S_ADC_ACTION_START;
	hdr.len = 1;

	/* FIXME when ready use helper function from mockturtle library
	that sends messsages */
	trtl_message_pack(&msg, &hdr, &streamid);
	if (hdr.flags & TRTL_PROTO_FLAG_SYNC) {
		return trtl_hmq_send_and_receive_sync(d3s->hmq,
						      (hdr.slot_io & 0xF),
						      &msg, 3000) >= 0 ? 0 : -1;
	}
	return -1;
}

int d3s_stream_stop(struct d3s_node *dev)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	struct trtl_msg msg;
	struct trtl_proto_header hdr = d3s_in_hdr_sync;

	hdr.msg_id = D3S_ADC_ACTION_STOP;

	/* FIXME when ready use helper function from mockturtle library
	that sends messsages */
	trtl_message_pack(&msg, &hdr, NULL);
	if (hdr.flags & TRTL_PROTO_FLAG_SYNC) {
		return trtl_hmq_send_and_receive_sync(d3s->hmq,
						      (hdr.slot_io & 0xF),
						      &msg, 3000) >= 0 ? 0 : -1;
	}

	return -1;
}

int d3s_stream_is_running(struct d3s_node *dev)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	struct d3s_adc_dev d3s_rt;
	struct trtl_proto_header hdr = d3s_in_hdr_sync;
	struct trtl_structure_tlv tlv = {
		.index = D3S_ADC_STRUCT_DEVICE,
		.size = sizeof(struct d3s_adc_dev),
		.structure = &d3s_rt,
	};
	int err;

	err = trtl_rt_structure_get(d3s->trtl, &hdr, &tlv, 1);
	if (err)
		return err;

	return !!(d3s_rt.flags & D3S_ADC_FLAG_RUNNING);
}


/**
 * It retrieves the current status
 * @param[in] dev device token
 * @param[out] status device descriptor on firmware
 * @return 0 on success, -1 on error and errno is appropriately set
 */
int d3s_status(struct d3s_node *dev, struct d3s_adc_dev *status)
{
	struct d3s_desc *d3s = (struct d3s_desc *)dev;
	struct trtl_proto_header hdr = d3s_in_hdr_sync;
	struct trtl_structure_tlv tlv = {
		.index = D3S_ADC_STRUCT_DEVICE,
		.size = sizeof(struct d3s_adc_dev),
		.structure = status,
	};

	return trtl_rt_structure_get(d3s->trtl, &hdr, &tlv, 1);
}
