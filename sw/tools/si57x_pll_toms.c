#include <stdio.h>
#include <unistd.h>

#include "si570_if_wb.h"
#include "wr_d3s_adc.h"
#include "i2c.h"

//static volatile struct SI570_WB *si570;

#define SI57X_ADDR 0x55

extern uint32_t svec_read(uint32_t addr);
extern void svec_write(uint32_t data, uint32_t addr);

#define BASE_SI57X 0x11000
#define BASE_D3S_ADC 0x10000

static int aux_scl(int state)
{

	int value = (svec_read(SI570_REG_GPSR + BASE_SI57X) & SI570_GPSR_SCL) ? 1 : 0; 
	if(state)
		svec_write(SI570_GPSR_SCL, SI570_REG_GPSR + BASE_SI57X);
	else
		svec_write(SI570_GPCR_SCL, SI570_REG_GPCR + BASE_SI57X);

	return value;
}

static int aux_sda(int state)
{
	int value = (svec_read(SI570_REG_GPSR + BASE_SI57X) & SI570_GPSR_SDA) ? 1 : 0; 

	if(state)
		svec_write(SI570_GPSR_SDA, SI570_REG_GPSR + BASE_SI57X);
	else
		svec_write(SI570_GPCR_SDA, SI570_REG_GPCR + BASE_SI57X);
	return value;
}


void si57x_init()
{

 	
 	mi2c_set_interface(0, aux_scl, aux_sda);
 	mi2c_scan(0);
}

uint8_t si57x_read_reg(uint8_t reg_addr)
{
	uint8_t rv;
 	mi2c_start(0);
 	mi2c_put_byte(0, SI57X_ADDR << 1);
	mi2c_put_byte(0, reg_addr);
 	mi2c_repeat_start(0);
 	mi2c_put_byte(0, (SI57X_ADDR << 1) | 1);
 	mi2c_get_byte(0, &rv, 1);
 	mi2c_stop(0);
 	return rv;
}

void si57x_write_reg(uint8_t reg_addr, uint8_t value)
{
 	mi2c_start(0);
 	mi2c_put_byte(0, SI57X_ADDR << 1);
	mi2c_put_byte(0, reg_addr);
	mi2c_put_byte(0, value);
 	mi2c_stop(0);
}

uint64_t si57x_read_rfreq()
{
  // RFREQ is 38 bits long. It is stored in registers 8,9,10,11 and 12.
  // LSB bits are in register 12.
  // MSB bits are bits 5 downto 0 of register 8.

  uint64_t rfreq = (((uint64_t) si57x_read_reg(12))  << 0) | 
		   (((uint64_t) si57x_read_reg(11))  << 8) | 
		   (((uint64_t) si57x_read_reg(10)) << 16) | 
		   (((uint64_t) si57x_read_reg(9))  << 24) | 
		   (((uint64_t) (si57x_read_reg(8) & 0x3f)) << 32);
  
  printf("Rfreq read = %010lx \n", rfreq);

  return(rfreq);
}

void si57x_read_dividers(uint8_t *n1, uint8_t *hsdiv)
{
 // HS_DIV is 3 bits long. It is stored in bits 7 downto 5 of register 7.
 // It is coded,taking values from 0 to 11. 

 // N1 is 7 bits long. It is stored split between registers 7 and 8.
 // N1[1:0] is stored in register 8, bits 7 downto 6.
 // N1[6:2] is stored in register 8, bits 4 downto 0.
 // Register value is -1 the divider value.
 
 	uint8_t r7, r8;
 	
 	r7 = si57x_read_reg(7);
 	r8 = si57x_read_reg(8);
 	*n1 = ( ((r7 & 0x1f) << 2) | ((r8 >> 6) & 0x3) ) + 1;
 	 	
 	switch(r7 >> 5) // hsdiv
 	{
 	    case 0:*hsdiv = 4; break;
 	    case 1:*hsdiv = 5; break;
 	    case 2:*hsdiv = 6; break;
 	    case 3:*hsdiv = 7; break;
 	    case 4:*hsdiv = 0; break;
 	    case 5:*hsdiv = 9; break;
 	    case 6:*hsdiv = 0; break;
 	    case 7:*hsdiv = 11; break;
 	}	
}

void si57x_set_hsdiv(int hsdiv)
{
	si57x_write_reg(137, 0x10); //freeze DCO
	si57x_write_reg(7, (si57x_read_reg(7) & 0x1f) | (hsdiv << 5));
	si57x_write_reg(137, 0x0); //un-freeze DCO
}

uint8_t si57x_read_n1_lsb()
{
	return si57x_read_reg(8) >> 6;
}

void si57x_set_base_rfreq(uint64_t rfreq)
{
    svec_write( (uint32_t)(rfreq >> 32) | (si57x_read_n1_lsb() << 6), SI570_REG_RFREQH + BASE_SI57X);
    svec_write( (uint32_t)(rfreq & 0xffffffffULL), SI570_REG_RFREQL + BASE_SI57X);
}


double si57x_get_fxtal(double f_out)
{
    uint8_t n1, hsdiv;

    double rf=(double)si57x_read_rfreq() / (double)(1<<28);


//    printf("rf %.1f\n", rf);
    si57x_read_dividers(&n1, &hsdiv);
    
    double f_xtal = f_out * (double)hsdiv * (double) n1 / rf;
    
    return f_xtal;    
}

uint8_t saved_r7, saved_r8;

double saved_rfreq;

void si57x_adjust_rfreq(double value)
{
    uint64_t rfreq_new = (saved_rfreq + value);

    printf("frreq_new %016lx\n", rfreq_new);
	uint8_t r7  = saved_r7;
	uint8_t r8 = saved_r8 | (uint32_t)(rfreq_new >> 32);
	uint8_t r9 = (rfreq_new >> 24) & 0xff;
	uint8_t r10 = (rfreq_new >> 16) & 0xff;
	uint8_t r11 = (rfreq_new >> 8) & 0xff;
	uint8_t r12 = (rfreq_new >> 0) & 0xff;

	si57x_write_reg(135, 1<<5);
		si57x_write_reg(7, r7);
		si57x_write_reg(8, r8);
		si57x_write_reg(9, r9);
		si57x_write_reg(10, r10);
		si57x_write_reg(11, r11);
		si57x_write_reg(12, r12);
	si57x_write_reg(135, 0);

		printf("%02x %02x %02x %02x %02x %02x\n", r7, r8, r9, r10, r11, r12);

}

int si57x_set_frequency(uint64_t f_out_ref, uint64_t f_out_new)
{
    uint64_t rf = si57x_read_rfreq() ;  
    double rf_float = (double) rf / (double)(1<<28);  // converts fixedpoint to double
    // rf is 38 bits long. It uses Q10.28 format. 
    // Values [0-1023.999999996]. Resolution:3.7e-9.

    printf("RF (fixed point) %010lx, rf (float) = %.6f \n", rf, rf_float );  // For debugging at host
    //printf("RF (fixed point) %010lx \n", rf );
    
    uint8_t n1, hsdiv;
    si57x_read_dividers(&n1, &hsdiv);

    double f_xtal_float = (double)f_out_ref * (double)hsdiv * (double)n1 / rf_float;

    // Max value of hsdiv=11 -> 4 bits, it is always an integer.
    // Max value of n1=(2^7-1)=127  -> 7 bits, it is always an integer.
    // The max. freq. of the Si570 is 1.4GHz. This number requires 31 bits to code it. 
    // Let's keep this input for the moment as an integer value 31 bits long.(Resolution 1Hz)

    uint64_t f_xtal = f_out_ref * hsdiv * n1 ;  // Requires 31+4+7 = 42 bits. Q42.0
    
    uint8_t RF_Q_MASK = 28;
//    f_xtal = (f_xtal / (rf>> 16))<<(RF_Q_MASK-16) ;    

    f_xtal = (f_xtal * (1ULL<<28)) / rf;

    printf("F_Xtal_int:%d\n", (int)f_xtal);
        
    printf("Current n1 %d, hsdiv %d, f_xtal %010lx (%.6f MHz) , f_xtal_float = %.6f MHz \n", n1, hsdiv, f_xtal, (double)(f_xtal)/1e6, f_xtal_float/1e6 );
    //printf("Current n1 %d, hsdiv %d, f_xtal %010lx \n", n1, hsdiv, f_xtal);
    
    int hsdiv_tab[] = {4,5,6,7,9,11, 0};
    int hsdiv_reg[] = {0,1,2,3,5,7, 0};
	
    n1 = 1;
    while (n1 <= 128)
    {
	int i;
	for(i = 0; hsdiv_tab[i]; i++)
	{
	    hsdiv = hsdiv_tab[i];
	    
            uint64_t f_dco_new = f_out_new * hsdiv * n1;  // Q42.0

            printf("f_dco: %010lx (asfloat %.6f GHz), n1 %d hsdiv %d\n", f_dco_new, (double)f_dco_new/1e9, n1, hsdiv);

            uint64_t F_DCO_MIN = 0x121152080;  // 4.85 GHz. From si57x datasheet
            uint64_t F_DCO_MAX = 0x151F55580;  // 5.67 GHz. From si57x datasheet
	    
            if(f_dco_new > F_DCO_MIN && f_dco_new < F_DCO_MAX)  
	    {
		
		printf (" ********** F_dco within valid range. Calculating new rffreq (n1 = %d hsdiv = %d) \n", n1, hsdiv) ;

		double rfreq_new_float = ((double)f_dco_new / (double)f_xtal_float) * (double) ( 1<< 28);

                uint64_t rfreq_new = ( f_dco_new << 28) / f_xtal; // (uint64_t) rfreq_new_float; // (f_dco_new) /((f_xtal) >> 12 ) ) << (RF_Q_MASK-12)  ;       
                                
                printf("rfreq_new = %010lx (%.6f), rfreq_new_float= %.6f \n", rfreq_new, (double)((double)(rfreq_new)/(1<<RF_Q_MASK)), rfreq_new_float) ;
                //printf("New: n1 %d, hsdiv %d, rfreq = %010lx \n", n1, hsdiv, rfreq_new ) ;

		si57x_write_reg(137, 0x10); //freeze DCO

		printf("hsdiv_reg %d\n",hsdiv_reg[hsdiv]);
		
		n1--;   // From datasheet: Register value is divider number-1.
		
		uint8_t  r7 = (hsdiv_reg[i] << 5) | (n1 >> 2);
		uint8_t  r8 = ((n1 & 0x3) << 6) | (uint32_t)(rfreq_new >> 32);
		uint8_t  r9 = (rfreq_new >> 24) & 0xff;
		uint8_t r10 = (rfreq_new >> 16) & 0xff;
		uint8_t r11 = (rfreq_new >> 8) & 0xff;
		uint8_t r12 = (rfreq_new >> 0) & 0xff;

		saved_r7 = r7;
		saved_r8 = (r8 & 0xc0);
	    
		saved_rfreq = (double)rfreq_new;

		printf("%02x %02x %02x %02x %02x %02x\n", r7, r8, r9, r10, r11, r12);

		si57x_write_reg(7, r7);
		si57x_write_reg(8, r8);
		si57x_write_reg(9, r9);
		si57x_write_reg(10, r10);
		si57x_write_reg(11, r11);
		si57x_write_reg(12, r12);

		si57x_write_reg(137, 0x0); //un-freeze DCO
		si57x_write_reg(135, 1<<6); //set NewFreq bit


		si57x_set_base_rfreq(rfreq_new);  // Save RFREQ in WB registers

        	uint64_t rf_new=si57x_read_rfreq();  //Read back for checking

		si57x_read_dividers(&n1, &hsdiv);
		
		printf("N1 = %d, HSDIV = %d RFnew %010lx %010lx \n", n1, hsdiv, rf_new, rfreq_new);
				
                return 0;
	    }
            /* 
            else
            {
                printf("New f_dco out of range. trying new HS_DIV \n");
            }  
            */
	}
    
	if (n1 == 1)
	    n1 = 2;
	else 
	    n1 += 2;
        
        // printf("New f_dco out of range. trying new N1: n1= %d \n" , n1);
    }

    return -1;
}

extern uint32_t svec_read(uint32_t addr);
extern void svec_write(uint32_t data, uint32_t addr);

void reset_core(void)
{
    svec_write( D3S_RSTR_PLL_RST , BASE_D3S_ADC + D3S_REG_RSTR);
    usleep(10000);
    svec_write(~D3S_RSTR_PLL_RST , BASE_D3S_ADC + D3S_REG_RSTR);
    usleep(10000);
                    
//    while(!(svec_read(BASE_D3S_ADC + D3S_REG_GPIOR) & D3S_GPIOR_SERDES_PLL_LOCKED ));
                        
                         //   WB register with SW reset not implemented. Do we need it?
                          //   dp_writel(DDS_RSTR_SW_RST, DDS_REG_RSTR); // trigger a software reset
                           //   delay(1000);
                            //   dp_writel(0, DDS_REG_RSTR);
                             //   delay(1000);
                                      sleep(1);
                                         // delay(1000000);
                                           
                                           }
                                                   
                                                   

extern void si57x_test()
{	
    double range = 200000.0;
 	printf("SI57x test\n");
 	si57x_init();
 	si57x_write_reg(135, 0x80); // reset
 	printf("After reset\n");
 	sleep(1);

 	si57x_set_frequency(100e6, 125e6);
	reset_core();

	int i,fr;
	sleep(3);
	double f_min, f_max, f_c = 125e6, err_min, err_max;

        fr = (uint32_t) svec_read(BASE_D3S_ADC + D3S_REG_WR_FREQ);
        printf("Center freq: %d \n", (unsigned int) fr);
	si57x_adjust_rfreq(range);

	sleep(3);

        f_max = fr = (uint32_t) svec_read(BASE_D3S_ADC + D3S_REG_WR_FREQ);
        printf("Max freq: %d \n", (unsigned int) fr);


	si57x_adjust_rfreq(-range);

	sleep(3);

        f_min = fr = (uint32_t) svec_read(BASE_D3S_ADC + D3S_REG_WR_FREQ);
        printf("Min freq: %d \n", (unsigned int) fr);

	err_min = (f_min - 125e6) / 125e6 * 1e6;
	err_max = (f_max - 125e6) / 125e6 * 1e6;
	
	printf("Pull range: %.0f / %.0f ppm\n", err_min, err_max);

	usleep(300); 	


#if 0

 	uint64_t rf=si57x_read_rfreq();
	printf("RF_H %08x RF_L %08x\n", (uint32_t)(rf >> 32), (uint32_t) rf);

	uint8_t n1, hsdiv;

	si57x_read_dividers(&n1, &hsdiv);
	printf("N1 = %d, HSDIV = %d\n", n1, hsdiv);

//	si57x_set_hsdiv(0);

	si57x_read_dividers(&n1, &hsdiv);
	printf("N1 = %d, HSDIV = %d\n", n1, hsdiv);
		

//	uint64_t rfreq_new = (uint64_t) ((125.0 / 100.0) * (double) rf);
//	si57x_set_base_rfreq(rfreq_new);
#endif

	//si57x_set_frequency(100e6, 125e6);

#if 0
    for(;;)
    {
	svec_write(0x0 | (1<<16), 0x60224);
	printf("%x\n", svec_read(0x40000));
//	si57x_adjust_rfreq(100e-6);
	sleep(2);
	svec_write(0xf000 | (1<<16), 0x60224);

//	si57x_adjust_rfreq(-100e-6);
	sleep(2);
	
    }
#endif

//    printf("f_xtal: %.10f Hz\n", si57x_get_fxtal(100.0e6));
}
