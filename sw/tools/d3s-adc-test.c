#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "wr_d3s_adc.h"
#include "d3s_acq_buffer_wb.h"

extern uint32_t svec_read(uint32_t addr);
extern void svec_write(uint32_t data, uint32_t addr);

#define BASE_D3S 0x10000
#define BASE_ACQ 0x10100

int gpior_get(uint32_t mask)
{
    return svec_read(BASE_D3S + D3S_REG_GPIOR) & mask ? 1 : 0;
    
}

void gpior_set(uint32_t mask, int value)
{
    uint32_t val = svec_read(BASE_D3S + D3S_REG_GPIOR);
    
    if(value)
	val |= mask;
    else
	val &= ~mask;
    
    return svec_write(val, BASE_D3S + D3S_REG_GPIOR);
}

void bb_spi_init()
{
    gpior_set(D3S_GPIOR_SPI_CS_ADC , 1);           
    gpior_set(D3S_GPIOR_SPI_SCK , 0);           
    gpior_set(D3S_GPIOR_SPI_MOSI , 0);           
}

static int delay(int us)
{
    usleep(us); // me lazy
    return 0;
}

/* Trivial bit-banged SPI access. Used to program the PLLs once during
   initialization and not touch them again ;) */
int bb_spi_txrx(uint32_t cs_mask, int nbits, uint32_t in, uint32_t *out)
{
    gpior_set(cs_mask, 0);
    delay(10);    
    
    int i;
    uint32_t rv = 0;

    for(i=nbits-1; i>=0 ;i--)
    {
	gpior_set(D3S_GPIOR_SPI_SCK, 0);
	delay(10);
	gpior_set(D3S_GPIOR_SPI_MOSI, in & (1 << i) ? 1 : 0);
	delay(10);
	gpior_set(D3S_GPIOR_SPI_SCK, 1);
	delay(10);
    	if (gpior_get(D3S_GPIOR_SPI_MISO))
	    rv |= (1 << i);

    }

    gpior_set(cs_mask, 1);
    delay(10);    

    *out = rv;

    return 0;
}


void setup_streaming(int max_error_deg, int max_run_length)
{
//    unsigned(rl_phase(13 downto 0) & "000000000");
    
    
    int max_err = (1<<(9+14))  * max_error_deg / 360;
    
    printf("MaxErr: %d\n", max_err);

    svec_write( max_err,              BASE_D3S + D3S_REG_RL_ERR_MAX);
    svec_write(-max_err,              BASE_D3S + D3S_REG_RL_ERR_MIN);
    
    svec_write(max_run_length,        BASE_D3S + D3S_REG_RL_LENGTH_MAX);

    svec_write( 50, BASE_D3S + D3S_REG_TRANSIENT_THRESHOLD_PHASE);
    svec_write(  6, BASE_D3S + D3S_REG_TRANSIENT_THRESHOLD_COUNT);

    svec_write(D3S_ADC_CSR_CLEAR_BUS, BASE_D3S + D3S_REG_ADC_CSR);
    svec_write(D3S_ADC_CSR_CLEAR_BUS, BASE_D3S + D3S_REG_ADC_CSR);

    printf("FIFO CSR: %x\n", svec_read( BASE_D3S + D3S_REG_ADC_CSR));
    svec_write(0,       BASE_D3S+D3S_REG_CR);
    svec_write(0,       BASE_D3S+D3S_REG_ADC_CSR);


    for (;;)
    {
	    uint32_t fifo_csr = svec_read( BASE_D3S+D3S_REG_ADC_CSR);
    	if(fifo_csr & D3S_ADC_CSR_EMPTY)
    	    break;
	
    	
    	volatile int fifo_r0 = svec_read( BASE_D3S+D3S_REG_ADC_R0);
    	printf("Fifo R0 = %d\n", fifo_r0 );
        //volatile int fifo_r1 = svec_read( BASE_D3S+D3S_REG_ADC_R1);
	    //volatile int fifo_r2 = svec_read( BASE_D3S+D3S_REG_ADC_R2);

    }


    svec_write(D3S_CR_ENABLE,       BASE_D3S+D3S_REG_CR);
    
	int i;
    
    for(i=0;i<100;)
    {
	uint32_t fifo_csr = svec_read( BASE_D3S+D3S_REG_ADC_CSR);
	
	   if(!(fifo_csr & D3S_ADC_CSR_EMPTY))
	   {
	        uint32_t fifo_r0 = svec_read( BASE_D3S+D3S_REG_ADC_R0);
            printf("Fifo R0 = %d, i= %d\n", fifo_r0, i );
	       
            //uint32_t fifo_r1 = svec_read( BASE_D3S+D3S_REG_ADC_R1);
 	        //uint32_t fifo_r2 = svec_read( BASE_D3S+D3S_REG_ADC_R2);

	       i++;
	       /*
            int is_rl = fifo_r0 & 0x80000000 ? 1: 0;
	    
	        if(is_rl)
	        {
		      uint32_t length = D3S_ADC_R1_RL_LENGTH_R(fifo_r1);
		      uint32_t dphase = D3S_ADC_R2_RL_PHASE_R(fifo_r2);
		      printf("Rl len %d dphase %d\n", length, dphase);
	        } else {
		      uint32_t phase = D3S_ADC_R2_RL_PHASE_R(fifo_r2);
		      printf("Non-RL phase %d\n", phase);
	        }
            */
	    
	   }
    }

}

void make_acq( char *filename, uint32_t acq_base, int bits )
{
    int i;
    uint32_t rx;
    int N;
    //svec_write(ACQ_CR_START, acq_base + ACQ_REG_CR);
    //while(!(svec_read( acq_base + ACQ_REG_CR) & ACQ_CR_READY ));

    FILE *f=fopen(filename,"wb");
    if (!f) {
        printf("Error opening the file %s\n", filename);
        return;
    }

    switch (acq_base) {
    /* buffers have different sizes now */
    case 0x10100:
                N=1024;
                break;
    case 0x10200:
                N=1024;
                break;
    case 0x10300:
                N=1024;
                break;
    case 0x10500:
                N=1024;
                break;
    }


    for(i=0;i<N;i++)
    {
        svec_write(i, acq_base + ACQ_REG_ADDR);
        rx = svec_read(acq_base + ACQ_REG_DATA);

	if(bits)
	
	    if (rx & (1<<(bits-1))) rx |= ~((1<<bits)-1);


        
    	fprintf(f, "%d %d\n", i, rx);    
    }
    
    fclose(f);

}
extern void si57x_test();

int main(void)
{
    printf("ID : %x\n",  svec_read(0));
//    uint32_t rx, tx;
//    int i;
    

    //si57x_test();

/*    svec_write(D3S_TCR_WR_LOCK_EN,    BASE_D3S + D3S_REG_TCR);


    bb_spi_init();
    
    	// Force 2's complement data output (register 1, bit 5) 
	bb_spi_txrx(D3S_GPIOR_SPI_CS_ADC, 16, (1 << 8) | (1 << 5), &rx);

		// Check the current configuration of the ADC chip 
		for (i = 0; i < 5; i++) {
			tx = 0x8000 | (i << 8);
			bb_spi_txrx(D3S_GPIOR_SPI_CS_ADC, 16, tx, &rx);
			printf("LTC register %02x: 0x%02x\n",
				i, rx & 0xff);
		}

*/

    uint32_t cr = svec_read(0x10100 + ACQ_REG_CR); 

    svec_write( cr | ACQ_CR_START, 0x10100 + ACQ_REG_CR); 
    //svec_write( cr | ACQ_CR_START, 0x10200 + ACQ_REG_CR); 
    //svec_write( cr | ACQ_CR_START, 0x10300 + ACQ_REG_CR);
    //svec_write( cr | ACQ_CR_START, 0x10500 + ACQ_REG_CR);

    // Buffer is nos cicular and freezed with a signal
    while(!(svec_read( 0x10100 + ACQ_REG_CR) & ACQ_CR_READY )); 
    while(!(svec_read( 0x10200 + ACQ_REG_CR) & ACQ_CR_READY )); 
    while(!(svec_read( 0x10300 + ACQ_REG_CR) & ACQ_CR_READY )); 
    while(!(svec_read( 0x10500 + ACQ_REG_CR) & ACQ_CR_READY )); 

    make_acq("/tmp/adc_data.dat", 0x10100, 14);  //14);
    printf("Data from 1st buffer acquired \n");

    make_acq("/tmp/raw_dphase.dat", 0x10200, 16);
    printf("Data from 2nd buffer acquired \n");

    make_acq("/tmp/raw_phase.dat", 0x10300, 16);
    printf("Data from 3rd buffer acquired\n");

    make_acq("/tmp/rl_state.dat", 0x10500, 2);
    printf("Data from 4th buffer acquired \n");

//    make_acq("/tmp/filter.dat", 0x10200, 14);
//    make_acq("/tmp/phase.dat", 0x10300, 0);
    
//    setup_streaming(3, 10000);
    
    return 0;

}
