#!/bin/bash

#scp ecalvo@cfv-774-cdv15:/tmp/adc.dat .
#scp ecalvo@cfv-774-cdv15:/tmp/filter.dat .
#scp ecalvo@cfv-774-cdv15:/tmp/phase.dat .

gnuplot  -e "plot 'tmpdata/20161027_new_card_AWG_adc_data.txt' using 1:2 with lines; pause -1"
gnuplot  -e "plot 'tmpdata/20161027_new_card_AWG_phase_data.txt' using 1:2 with lines; pause -1"
#gnuplot  -e "plot 'phase.dat' using 1:2 with lines; pause -1"
