#include <stdio.h>
#include <unistd.h>

#include "si570_if_wb.h"
#include "wr_d3s_adc.h"
#include "i2c.h"

//static volatile struct SI570_WB *si570;

#define SI57X_ADDR 0x55

extern uint32_t svec_read(uint32_t addr);
extern void svec_write(uint32_t data, uint32_t addr);

#define BASE_SI57X 0x11000
#define BASE_D3S_ADC 0x10000

static int aux_scl(int state)
{

	int value = (svec_read(SI570_REG_GPSR + BASE_SI57X) & SI570_GPSR_SCL) ? 1 : 0; 
	if(state)
		svec_write(SI570_GPSR_SCL, SI570_REG_GPSR + BASE_SI57X);
	else
		svec_write(SI570_GPCR_SCL, SI570_REG_GPCR + BASE_SI57X);

	return value;
}

static int aux_sda(int state)
{
	int value = (svec_read(SI570_REG_GPSR + BASE_SI57X) & SI570_GPSR_SDA) ? 1 : 0; 

	if(state)
		svec_write(SI570_GPSR_SDA, SI570_REG_GPSR + BASE_SI57X);
	else
		svec_write(SI570_GPCR_SDA, SI570_REG_GPCR + BASE_SI57X);
	return value;
}


void si57x_init_I2C()
{
 	mi2c_set_interface(0, aux_scl, aux_sda);
 	mi2c_scan(0);
}

uint8_t si57x_read_reg(uint8_t reg_addr)
{
	uint8_t rv;
 	mi2c_start(0);
 	mi2c_put_byte(0, SI57X_ADDR << 1);
	mi2c_put_byte(0, reg_addr);
 	mi2c_repeat_start(0);
 	mi2c_put_byte(0, (SI57X_ADDR << 1) | 1);
 	mi2c_get_byte(0, &rv, 1);
 	mi2c_stop(0);
 	return rv;
}

void si57x_write_reg(uint8_t reg_addr, uint8_t value)
{
 	mi2c_start(0);
 	mi2c_put_byte(0, SI57X_ADDR << 1);
	mi2c_put_byte(0, reg_addr);
	mi2c_put_byte(0, value);
 	mi2c_stop(0);
}

uint64_t si57x_read_rfreq()
{
  // RFREQ is 38 bits long. It is stored in registers 8,9,10,11 and 12.
  // LSB bits are in register 12.
  // MSB bits are bits 5 downto 0 of register 8.

  uint64_t rfreq = (((uint64_t) si57x_read_reg(12))  << 0) | 
		   (((uint64_t) si57x_read_reg(11))  << 8) | 
		   (((uint64_t) si57x_read_reg(10)) << 16) | 
		   (((uint64_t) si57x_read_reg(9))  << 24) | 
		   (((uint64_t) (si57x_read_reg(8) & 0x3f)) << 32);
  
  //printf("Rfreq read = %010lx \n", rfreq);

  return(rfreq);
}

void si57x_read_dividers(uint8_t *n1, uint8_t *hsdiv)
{
 // HS_DIV is 3 bits long. It is stored in bits 7 downto 5 of register 7.
 // It is coded,taking values from 0 to 11. 

 // N1 is 7 bits long. It is stored split between registers 7 and 8.
 // N1[1:0] is stored in register 8, bits 7 downto 6.
 // N1[6:2] is stored in register 8, bits 4 downto 0.
 // Register value is -1 the divider value.
 
 	uint8_t r7, r8;
 	
 	r7 = si57x_read_reg(7);
 	r8 = si57x_read_reg(8);
 	*n1 = ( ((r7 & 0x1f) << 2) | ((r8 >> 6) & 0x3) ) + 1;
 	 	
 	switch(r7 >> 5) // hsdiv
 	{
 	    case 0:*hsdiv = 4; break;
 	    case 1:*hsdiv = 5; break;
 	    case 2:*hsdiv = 6; break;
 	    case 3:*hsdiv = 7; break;
 	    case 4:*hsdiv = 0; break;
 	    case 5:*hsdiv = 9; break;
 	    case 6:*hsdiv = 0; break;
 	    case 7:*hsdiv = 11; break;
 	}	
}

void si57x_set_hsdiv(int hsdiv)
{
	si57x_write_reg(137, 0x10); //freeze DCO
	si57x_write_reg(7, (si57x_read_reg(7) & 0x1f) | (hsdiv << 5));
	si57x_write_reg(137, 0x0); //un-freeze DCO
}

uint8_t si57x_read_n1_lsb()
{
	return si57x_read_reg(8) >> 6;
}

void si57x_set_base_rfreq(uint64_t rfreq)
{
    svec_write( (uint32_t)(rfreq >> 32) | (si57x_read_n1_lsb() << 6), SI570_REG_RFREQH + BASE_SI57X);
    svec_write( (uint32_t)(rfreq & 0xffffffffULL), SI570_REG_RFREQL + BASE_SI57X);
}


uint64_t si57x_get_fxtal(double f_out, uint8_t *n1, uint8_t *hsdiv)
{

    uint64_t rf_int =  si57x_read_rfreq();
    double   rf_double = (double) rf_int / (double)(1<<28);
    printf("RF_H %08x RF_L %08x  (%.10f)\n", (uint32_t)(rf_int>>32), (uint32_t) rf_int, rf_double);

    si57x_read_dividers(n1, hsdiv);
    printf("N1 = %d, HSDIV = %d\n", *n1, *hsdiv);

    double f_xtal_double = f_out * (double) (*hsdiv) * (double) (*n1) / rf_double;
    printf("Fxtal float: %.10f \n", f_xtal_double);

    uint64_t f_xtal = ((uint64_t)f_out) * (*hsdiv) * (*n1) ; 
    f_xtal = (f_xtal * (1ULL<<28)) / rf_int;

    printf("Fxtal FP: %.10f, (RF_H %08x RF_L %08x) \n", (double) f_xtal, (uint32_t)(f_xtal>>32), (uint32_t) f_xtal);
    
    return f_xtal;    
}

uint8_t saved_r7, saved_r8;

double saved_rfreq;

void si57x_adjust_rfreq(double percentage)
{
    uint64_t rfreq_new = (saved_rfreq * (1.0 + percentage));
    

    printf("frreq_new %016lx\n", rfreq_new);
	uint8_t r7  = saved_r7;
	uint8_t r8 = saved_r8 | (uint32_t)(rfreq_new >> 32);
	uint8_t r9 = (rfreq_new >> 24) & 0xff;
	uint8_t r10 = (rfreq_new >> 16) & 0xff;
	uint8_t r11 = (rfreq_new >> 8) & 0xff;
	uint8_t r12 = (rfreq_new >> 0) & 0xff;

	si57x_write_reg(135, 1<<5);
		si57x_write_reg(7, r7);
		si57x_write_reg(8, r8);
		si57x_write_reg(9, r9);
		si57x_write_reg(10, r10);
		si57x_write_reg(11, r11);
		si57x_write_reg(12, r12);
	si57x_write_reg(135, 0);

		printf("%02x %02x %02x %02x %02x %02x\n", r7, r8, r9, r10, r11, r12);

}

int si57x_set_frequency(uint64_t f_out_ref, uint64_t f_out_new)
{
    uint8_t n1, hsdiv;
    uint64_t f_xtal = si57x_get_fxtal(f_out_ref, &n1, &hsdiv);
    
    // Find new DCO value and divisors
    int hsdiv_tab[] = {4,5,6,7,9,11, 0};
    int hsdiv_reg[] = {0,1,2,3,5,7, 0};
	
    n1 = 1;
    while (n1 <= 128)
    {
	int i;
	for(i = 0; hsdiv_tab[i]; i++)
	{
	    hsdiv = hsdiv_tab[i];
	    
            uint64_t f_dco_new = f_out_new * hsdiv * n1;  
            uint64_t F_DCO_MIN = 0x121152080;  // 4.85 GHz. From si57x datasheet
            uint64_t F_DCO_MAX = 0x151F55580;  // 5.67 GHz. From si57x datasheet
	    
            if(f_dco_new > F_DCO_MIN && f_dco_new < F_DCO_MAX)  
	    {
		
  		        printf (" ********** F_dco within valid range. Calculating new rffreq \n") ;
                printf("f_dco: %010lx (asfloat %.6f GHz), n1 %d hsdiv %d\n", f_dco_new, (double)f_dco_new/1e9, n1, hsdiv);

		        double rfreq_new_float = ((double)f_dco_new / (double)f_xtal);
                uint64_t rfreq_new =  (f_dco_new<<28) /(f_xtal) ;       
                                
                printf("rfreq_new_FP = %010lx (%.6f), rfreq_new_float= %.6f \n", rfreq_new,(double) (rfreq_new), rfreq_new_float) ;

        // Now program the Si57x with the values found
		si57x_write_reg(137, 0x10); //freeze DCO

		printf("hsdiv_reg %d\n",hsdiv_reg[hsdiv]);
		
		n1--;   // From datasheet: Register value is divider number-1.
		
		uint8_t  r7 = (hsdiv_reg[i] << 5) | (n1 >> 2);
		uint8_t  r8 = ((n1 & 0x3) << 6) | (uint32_t)(rfreq_new >> 32);
		uint8_t  r9 = (rfreq_new >> 24) & 0xff;
		uint8_t r10 = (rfreq_new >> 16) & 0xff;
		uint8_t r11 = (rfreq_new >> 8) & 0xff;
		uint8_t r12 = (rfreq_new >> 0) & 0xff;

		saved_r7 = r7;
		saved_r8 = (r8 & 0xc0);
	    
		saved_rfreq = (double)rfreq_new;

		printf("%02x %02x %02x %02x %02x %02x\n", r7, r8, r9, r10, r11, r12);

		si57x_write_reg(7, r7);
		si57x_write_reg(8, r8);
		si57x_write_reg(9, r9);
		si57x_write_reg(10, r10);
		si57x_write_reg(11, r11);
		si57x_write_reg(12, r12);

		si57x_write_reg(137, 0x0); //un-freeze DCO
		si57x_write_reg(135, 1<<6); //set NewFreq bit


		si57x_set_base_rfreq(rfreq_new);  // Save RFREQ in WB registers

        	uint64_t rf_new=si57x_read_rfreq();  //Read back for checking

		si57x_read_dividers(&n1, &hsdiv);
		
		printf("N1 = %d, HSDIV = %d RFnew %010lx %010lx \n", n1, hsdiv, rf_new, rfreq_new);
				
                return 0;
	    }
            /* 
            else
            {
                printf("New f_dco out of range. trying new HS_DIV \n");
            }  
            */
	}
    
	if (n1 == 1)
	    n1 = 2;
	else 
	    n1 += 2;
        
        // printf("New f_dco out of range. trying new N1: n1= %d \n" , n1);
    }

    return -1;
}

extern uint32_t svec_read(uint32_t addr);
extern void svec_write(uint32_t data, uint32_t addr);


extern void si57x_test()
{	
    printf("SI57x test\n");
 	si57x_init_I2C();

 	si57x_write_reg(135, 0x80); // reset Si57x
 	printf("After reset\n");
 	sleep(3);

     	si57x_set_frequency(100e6, 125e6);

	int i,fr;
	for (i=0; i < 2; i++)
    {
    	sleep(1);

        fr = (uint32_t) svec_read(BASE_D3S_ADC + D3S_REG_WR_FREQ);
        printf("%d: Measured WR freq: %d \n", i, (unsigned int) fr);
    //    acc = (uint64_t) acc + fr;
        
    }

	usleep(300); 	


#if 0

 	uint64_t rf=si57x_read_rfreq();
	printf("RF_H %08x RF_L %08x\n", (uint32_t)(rf >> 32), (uint32_t) rf);

	uint8_t n1, hsdiv;

	si57x_read_dividers(&n1, &hsdiv);
	printf("N1 = %d, HSDIV = %d\n", n1, hsdiv);

//	si57x_set_hsdiv(0);

	si57x_read_dividers(&n1, &hsdiv);
	printf("N1 = %d, HSDIV = %d\n", n1, hsdiv);
		

//	uint64_t rfreq_new = (uint64_t) ((125.0 / 100.0) * (double) rf);
//	si57x_set_base_rfreq(rfreq_new);
#endif

	//si57x_set_frequency(100e6, 125e6);

#if 0
    for(;;)
    {
	svec_write(0x0 | (1<<16), 0x60224);
	printf("%x\n", svec_read(0x40000));
//	si57x_adjust_rfreq(100e-6);
	sleep(2);
	svec_write(0xf000 | (1<<16), 0x60224);

//	si57x_adjust_rfreq(-100e-6);
	sleep(2);
	
    }
#endif

//    printf("f_xtal: %.10f Hz\n", si57x_get_fxtal(100.0e6));
}
