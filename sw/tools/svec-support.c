/* A simple console for accessing the SVEC virtual UART (i.e. for communicating with the WR Core shell
   from a Linux terminal. */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <getopt.h>
#include <errno.h>


static int addr_fd;
static int data_fd;

static int lun = 0;

extern uint32_t svec_read(uint32_t addr)
{
    char str[1024];
    sprintf(str,"/sys/bus/vme/devices/svec.%d/vme_addr", lun);
    addr_fd = open(str, O_RDWR);
    sprintf(str,"/sys/bus/vme/devices/svec.%d/vme_data", lun);
    data_fd = open(str, O_RDWR);
    if(addr_fd < 0 || data_fd < 0)
    {
	fprintf(stderr, "can't open SVEC at LUN %d: ", lun);
	perror("open()");
	exit(-1);
    }

    sprintf(str,"0x%x\n", addr);
    write(addr_fd, str, strlen(str) + 1);
    int n = read(data_fd, str, sizeof(str));
    uint32_t data;
    close(addr_fd);
    close(data_fd);
    if(n <= 0)
    {
	perror("read()");
	exit(-1);
    }
    
    str[n] = 0;
    sscanf(str, "%i", &data);
    return data;
}


extern void svec_write(uint32_t data, uint32_t addr)
{
    char str[256];
    sprintf(str,"/sys/bus/vme/devices/svec.%d/vme_addr", lun);
    addr_fd = open(str, O_RDWR);
    sprintf(str,"/sys/bus/vme/devices/svec.%d/vme_data", lun);
    data_fd = open(str, O_RDWR);
    if(addr_fd < 0 || data_fd < 0)
    {
	fprintf(stderr, "can't open SVEC at LUN %d: ", lun);
	perror("open()");
	exit(-1);
    }
    sprintf(str,"0x%x\n", addr);
    write(addr_fd, str, strlen(str) + 1);
    sprintf(str,"0x%x\n", data);
    write(data_fd, str, strlen(str) + 1);
    close(addr_fd);
    close(data_fd);
}

