#!/bin/bash

D3S=../

if [ "x$TRTL" = "x" ]
then
    TRTL=$D3S/sw/mock-turtle-sw
fi

for DEV in $($TRTL/tools/mockturtle-list)
do
    devid=$(echo $DEV | cut -d '-' -f 2)
    echo -e "\nLoading MASTER application in devid=($devid)"
    $TRTL/tools/mockturtle-loader -D 0x$devid -i 0 -f $D3S/sw/rt/d3s_adc_master/d3s_adc_master.bin

    echo -e "\nLoading SLAVE application in devid=($devid)"
    $TRTL/tools/mockturtle-loader -D 0x$devid -i 1 -f $D3S/sw/rt/d3s_adc_slave/d3s_adc_slave.bin

    echo -e "\nRestarting MT CPUs"
    $TRTL/tools/mockturtle-cpu-restart -D 0x$devid -i 0 -i 1
done
