#!/bin/bash

LUNS=""
while getopts hL: o
do      case $o in
        L)      LUNS="$LUNS$OPTARG ";;
        [h?])   echo >&2 "usage: $0 [-h] [-L lun]"
                exit ;;
        esac
done

if [ "x$LUNS" = "x" ]
then
    echo "Specify the SVEC LUNs to be used"
    exit 1
fi

D3S=../
if [ "x$GATEWARE" = "x" ]
then
    GATEWARE=$D3S/hdl/syn/svec/wr_d3s_adc_test/svec_top.bin
fi

if [ "x$TRTL" = "x" ]
then
    TRTL=$D3S/sw/mock-turtle-sw
fi

# Show GW filename and date
modDate=$(stat -c %y "$GATEWARE") 
#%y = last modified. Qoutes are needed otherwise spaces in file name with give error of "no such file"
modDate=${modDate%% *} 
#%% takes off everything off the string after the date to make it look pretty

echo "Remove and then Load drivers (svec, mockturtle)"
mount -t debugfs none /sys/kernel/debug/
rmmod mockturtle svec
(cd /usr/local/drivers/svec/; sh install_svec.sh)
insmod $TRTL/kernel/mockturtle.ko

for N in $LUNS
do
    echo "Loading D3S bitstream on SVEC.$N"
    echo "Gateware file ($modDate): $GATEWARE"
    if [ -e /dev/svec.$N ]
    then
        dd if=$GATEWARE of=/dev/svec.$N obs=10M
    else
        echo "Cannot program svec.$N, probably you are using an old SVEC driver"
        exit 1
    fi
done

echo "Loading White Rabbit core"
/usr/local/bin/svec-wrc-loader -a /acc/local/share/firmware/wr-core/wr-core-20150620-lowoffset.bin

sh $D3S/sw/load_d3s.sh
