/**
 * This is the real-time application to the Master
 */

#include "d3s_adc_master.h"

#define RISE_EDGE_DET 1         // Trigger time stamping sensible only to rising edges
#define ENC_FIFO_SIZE 16384     // FW D3S encoder fifo size

struct d3s_adc_dev d3s = {
    .stream_id = 0,
    .mode = D3S_MODE_MASTER,
    .flags = 0,
    .stream_enabled = 0,
    .err_max = 0,
    .rl_length_max = 0,
    .trans_thr_phase = 0,
    .trans_thr_count = 0,
    .last_Trev_ts_tai = 0,
    .last_Trev_ts_ns = 0,
    .last_Trev_period_ns = 0,
    .stats = {
        .master = {
            .enc_state = 7,
            .msg_failure = 0,
            .msg_rf_sent = 0,
            .msg_tr_sent = 0,
            .fifo_overflow = 0,
            .stdc_fifo_overflow = 0,
            .fifo_worst_count = 0,
        },
    },
};
struct rt_application app;

static uint32_t Trev_update =0;


/**
 * It enables/disables the sampling
 * @param[in] enable 1 to enable 0 to disable
 */
static inline void d3s_adc_master_enable(unsigned int enable)
{
    uint32_t cr;

    cr = dp_readl(BASE_D3S_ADC_MASTER + D3S_REG_CR);
    if (enable)
        cr |= D3S_CR_ENABLE;
    else
        cr &= ~D3S_CR_ENABLE;
    dp_writel(cr, BASE_D3S_ADC_MASTER + D3S_REG_CR);

    d3s.stream_enabled = dp_readl(BASE_D3S_ADC_MASTER + D3S_REG_CR) & D3S_CR_ENABLE;
}


/**
 * It retrieves a memory location where is possible to write a message.
 * Today we are using the shared memory loop, it will be something else
 * in the future
 */
static inline struct d3s_msg *d3s_adc_master_message_claim(void)
{
    return loop_queue_claim();
}


/**
 * It sends a given message.
 * Today we are using the shared memory loop, it will be something else
 * in the future
 */
static inline void d3s_adc_master_message_send(struct d3s_msg *msg)
{
    loop_queue_push();
}

/**
 * This function setups and starts the STDC module
 * Edge_detection decoding:
 *        = 0 disables edge detection
 *        = 1 enables rising edge detection
 *        = 2 enables falling edge detection
 *        = 3 enables rising and falling edge detection
 */
static void d3s_adc_master_stdc_init(void)
{
    // Clear the serdes_tdc FIFO
    dp_writel(STDC_CTRL_CLR | STDC_CTRL_CLR_OVF, BASE_D3S_STDC + STDC_REG_CTRL);

    pp_printf("STDC: Fifo cleared: %"PRIx32"\n",
              dp_readl(BASE_D3S_STDC + STDC_REG_CTRL));
    delay(5);

    // Enables the edge detection
    dp_writel(STDC_CTRL_FILTER_W(RISE_EDGE_DET), BASE_D3S_STDC + STDC_REG_CTRL);
    pp_printf("STDC: Edge filter configured: %"PRIx32"\n",
              dp_readl(BASE_D3S_STDC + STDC_REG_CTRL));
    delay(5);
}


/**
 * It gets the data from the ADC and it sends and RF packet over the network
 */
static inline void d3s_adc_master_send_rf_packet(void)
{
    struct d3s_msg *current_msg;
    uint32_t *current_payload, record;
    register int i;

    current_msg = d3s_adc_master_message_claim();
    if (!current_msg) {
        d3s.stats.master.msg_failure++;
        return;
    }

    current_msg->type = D3S_MSG_RF;
    current_msg->RF_payload.plen = RECORDS_PER_MESSAGE;
    current_payload = current_msg->RF_payload.RF_data;

    for (i = 0; i < RECORDS_PER_MESSAGE; i++) {
        record = dp_readl(BASE_D3S_ADC_MASTER + D3S_REG_ADC_R0);
        *current_payload = record;
        current_payload++;
    }
    d3s_adc_master_message_send(current_msg);
    d3s.stats.master.msg_rf_sent++;
}


/**
 * This function reads from the RF encoder fifo and sends the RF records
 */
static inline void d3s_adc_master_send_rf_packets(void)
{
    uint32_t csr, fifo_count;

    csr = dp_readl(BASE_D3S_ADC_MASTER + D3S_REG_ADC_CSR);
    fifo_count = csr & D3S_ADC_CSR_USEDW_MASK;
    if (csr & D3S_ADC_CSR_EMPTY || fifo_count < RECORDS_PER_MESSAGE)
        return;

    if (csr & D3S_ADC_CSR_FULL) {
        d3s.stats.master.fifo_overflow++;
        dp_writel(D3S_ADC_CSR_CLEAR_BUS, BASE_D3S_ADC_MASTER + D3S_REG_ADC_CSR);
        return;
    }

    if (fifo_count > d3s.stats.master.fifo_worst_count)
        d3s.stats.master.fifo_worst_count = fifo_count;

    d3s_adc_master_send_rf_packet();
}


/**
 * This function for reads from the STDC fifo and sends the Trev records
 */
static void d3s_adc_master_send_trev_packet(struct d3s_adc_dev *dev)
{
    struct d3s_msg *current_msg = NULL;  //msg structure used for the smem loop back test
    uint32_t ts_tai_h, ts_tai_l, last_Trev_ts_ns;
    uint64_t last_Trev_ts_tai;
    uint32_t ctl, stat;

    // Check if there is any event to process in the fifo
    stat = dp_readl(BASE_D3S_STDC + STDC_REG_STATUS);

    if (stat & (STDC_STATUS_EMPTY | STDC_STATUS_OVF)) {
        if (stat & STDC_STATUS_OVF)
            d3s.stats.master.stdc_fifo_overflow ++;
        return;
    }

    current_msg = d3s_adc_master_message_claim();  // returns pointer to buff[head] or NULL if full
    if (!current_msg)
        return;

    ts_tai_h = dp_readl(BASE_D3S_STDC + STDC_REG_TDC_TS_TAI_H);
    ts_tai_l = dp_readl(BASE_D3S_STDC + STDC_REG_TDC_TS_TAI_L);
    last_Trev_ts_ns  = dp_readl(BASE_D3S_STDC + STDC_REG_TDC_TS_NS);
    ctl  = dp_readl(BASE_D3S_STDC + STDC_REG_CTRL);
    dp_writel( ctl | STDC_CTRL_NEXT, BASE_D3S_STDC + STDC_REG_CTRL); // Process next event

    current_msg->type = D3S_MSG_TREV_UPDATE;
    current_msg->TREV_payload.ts_tai_h = ts_tai_h;
    current_msg->TREV_payload.ts_tai_l = ts_tai_l;
    current_msg->TREV_payload.ts_ns  = last_Trev_ts_ns;

    d3s_adc_master_message_send(current_msg);
    d3s.stats.master.msg_tr_sent++;

    // Keep statistics in the MASTER
    last_Trev_ts_tai = ts_tai_h;
    last_Trev_ts_tai <<= 32;
    last_Trev_ts_tai |= ts_tai_l;

    // Calculate Revolution period update
    if (Trev_update > 0 )
    {
        if (last_Trev_ts_tai == dev->last_Trev_ts_tai)
            dev->last_Trev_period_ns = last_Trev_ts_ns - dev->last_Trev_ts_ns ;
        else
            dev->last_Trev_period_ns = WR_CYCLE_MAX_VALUE_NS + last_Trev_ts_ns - dev->last_Trev_ts_ns ;
    }

    if (dev->last_Trev_period_ns < TREV_MIN || dev->last_Trev_period_ns > TREV_MAX)
        pp_printf("Wrong Trev Period %d\n", dev->last_Trev_period_ns);

    //Update last event to calculate next frev_period
    dev->last_Trev_ts_tai = last_Trev_ts_tai;
    dev->last_Trev_ts_ns  = last_Trev_ts_ns;

    Trev_update ++ ;
}


static void d3s_adc_master_update(struct d3s_adc_dev *dev)
{
    if (dev->stream_enabled == 0 || !wr_is_timing_ok(dev))
        return;

    d3s_adc_master_send_trev_packet(dev);
    d3s_adc_master_send_rf_packets();
    dev->stats.master.enc_state = dp_readl(BASE_D3S_ADC_MASTER + D3S_REG_CR) & D3S_CR_ENC_ST_MASK;

    return;
}

static void d3s_adc_master_setup_compression(struct d3s_adc_dev *dev)
{
    init_RL(dev);

    dp_writel(dev-> err_max,
              BASE_D3S_ADC_MASTER + D3S_REG_RL_ERR_MAX);
    dp_writel(-dev-> err_max,
              BASE_D3S_ADC_MASTER + D3S_REG_RL_ERR_MIN);
    dp_writel(dev-> rl_length_max,
              BASE_D3S_ADC_MASTER + D3S_REG_RL_LENGTH_MAX);
    dp_writel(dev-> trans_thr_phase,
              BASE_D3S_ADC_MASTER + D3S_REG_TRANSIENT_THRESHOLD_PHASE);
    dp_writel(dev-> trans_thr_count,
              BASE_D3S_ADC_MASTER + D3S_REG_TRANSIENT_THRESHOLD_COUNT);
}


/**
 * This function initializes the master data streaming process
 * after receiveing the 'start' message from the Host
 */
static int d3s_adc_master_start(struct trtl_proto_header *hin, void *pin,
                                struct trtl_proto_header *hout, void *pout)
{
    uint32_t *data = pin;

    d3s.stream_id = data[0];

    rt_send_ack(hin, pin, hout, pout);

    d3s_adc_master_enable(0);
    dp_writel(D3S_ADC_CSR_CLEAR_BUS, BASE_D3S_ADC_MASTER + D3S_REG_ADC_CSR);
    pp_printf("Encoder FIFO cleaned. CSR: %"PRIx32"\n",
              dp_readl( BASE_D3S_ADC_MASTER + D3S_REG_ADC_CSR));

    d3s_adc_master_stdc_init();

    d3s.stats.master.msg_failure = 0;
    d3s.stats.master.fifo_overflow = 0;
    d3s.stats.master.msg_rf_sent = 0;
    d3s.stats.master.msg_tr_sent = 0;
    d3s.stats.master.fifo_worst_count = 0;

    loop_queue_init();
    d3s_adc_master_enable(1);

    Trev_update =0;
    d3s.last_Trev_period_ns = 23270;

    pp_printf("Master streamming initialized.\n");

    return 0;
}

static int d3s_adc_master_stop(struct trtl_proto_header *hin, void *pin,
                               struct trtl_proto_header *hout, void *pout)
{
    d3s.stream_id = 0x0;
    d3s_adc_master_enable(0);
    pp_printf("Master stopped.\n");
    return 0;
}


#define PLL_LOCK_RETRY 1000
static int d3s_adc_master_reset_core(void)
{
    unsigned int retry = PLL_LOCK_RETRY;
    uint32_t gpior;

    dp_writel( D3S_RSTR_PLL_RST , BASE_D3S_ADC_MASTER + D3S_REG_RSTR);
    delay(1000);
    dp_writel(~D3S_RSTR_PLL_RST , BASE_D3S_ADC_MASTER + D3S_REG_RSTR);
    delay(1000);

    /*
     * Wait for the SERDES PLL to lock or fail if it doesnot after
     * a PLL_LOCK_RETRY times
     */
    do {
        gpior = dp_readl(BASE_D3S_ADC_MASTER + D3S_REG_GPIOR);
    } while(--retry> 0 && !(gpior & D3S_GPIOR_SERDES_PLL_LOCKED ));

    delay(1000000);

    if (retry == 0) {
        pp_printf("SERDES PLL does not lock.\n");
        return -1;
    }
    return 0;
}

static int d3s_adc_master_init()
{
    int err;

    wr_init(&d3s);
    setup_LTC2175();
    si57x_init();

    err = d3s_adc_master_reset_core();
    if (err)
        return err;

    d3s_adc_master_enable(0);
    dp_writel( D3S_ADC_CSR_CLEAR_BUS, BASE_D3S_ADC_MASTER + D3S_REG_ADC_CSR);

    d3s_adc_master_setup_compression(&d3s);
    init_TOF_vars(&d3s);

    // Configure diagnostic buffers in circular/non-circular mode
    dp_writel( 0x0,  BASE_ACQ_BUF1 + ACQ_REG_CR);
    dp_writel( 0x0,  BASE_ACQ_BUF2 + ACQ_REG_CR);
    dp_writel( 0x0,  BASE_ACQ_BUF3 + ACQ_REG_CR);

    pp_printf("Master GW initialized \n");
    return 0;
}



static struct rt_structure d3s_in_structures[] = {
    [D3S_ADC_STRUCT_DEVICE] = {
        .struct_ptr = &d3s,
        .len = sizeof(struct d3s_adc_dev),
    },
};


static struct rt_variable d3s_in_variables[] = {

};


static action_t *d3s_in_actions[] = {
        [RT_ACTION_RECV_PING] = rt_recv_ping,
        [RT_ACTION_RECV_VERSION] = rt_version_getter,
        [RT_ACTION_RECV_FIELD_SET] = rt_variable_setter,
        [RT_ACTION_RECV_FIELD_GET] = rt_variable_getter,
        [RT_ACTION_RECV_STRUCT_SET] = rt_structure_setter,
        [RT_ACTION_RECV_STRUCT_GET] = rt_structure_getter,
        [D3S_ADC_ACTION_START] = d3s_adc_master_start,
        [D3S_ADC_ACTION_STOP] = d3s_adc_master_stop,
};


struct rt_mq mq[] = {
    [D3S_IN_CONTROL] = {
        .index = 0,
        .flags = RT_MQ_FLAGS_LOCAL | RT_MQ_FLAGS_INPUT,
    },
};


struct rt_application app = {
        .name = "D3S ADC MASTER",
        .version = {
        .fpga_id = D3S_APP_ID,
                .rt_id = D3S_RT_ID_MASTER, // uint32 identifier, host library should know this number for validation
                .rt_version = RT_VERSION(0, 2),
                .git_version = GIT_VERSION
        },
        .mq = mq,
        .n_mq = 1,

        .structures = d3s_in_structures,
        .n_structures = ARRAY_SIZE(d3s_in_structures),

        .variables = d3s_in_variables,
        .n_variables = ARRAY_SIZE(d3s_in_variables),

        .actions = d3s_in_actions,
        .n_actions = ARRAY_SIZE(d3s_in_actions),
};


int main(void)
{
        int err;

        rt_init(&app);

        err = d3s_adc_master_init();
        if (err)
            return err;

        while (1) {
                rt_mq_action_dispatch(D3S_IN_CONTROL);
                wr_update_link(&d3s);
                d3s_adc_master_update(&d3s);
        }

        return 0;
}
