#ifndef __LTC2175_H__
#define __LTC2175_H__

#include <libmockturtle-rt.h>
#include "hw/wr_d3s_adc.h"

// FW Core address offsets
#define BASE_D3S_ADC 0             // Base address of D3S_ADC core

#endif
