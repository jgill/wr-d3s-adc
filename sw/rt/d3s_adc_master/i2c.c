/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2011,2012 CERN (www.cern.ch)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 * Author: Grzegorz Daniluk <grzegorz.daniluk@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

#include <stdio.h>
#include <stdint.h>

#include "i2c.h"

#define I2C_DELAY 300

static struct i2c_bitbang_interface i2c_if;

void mi2c_set_interface(int (*scl)(),  int (*sda)())
{
	i2c_if.scl = scl;
	i2c_if.sda = sda;
	scl(1);
	sda(1);
}


void mi2c_delay()
{
	int i;
	for (i = 0; i < I2C_DELAY; i++)
		asm volatile ("nop");
}

#define M_SDA_OUT(x) { i2c_if.sda(x); mi2c_delay(); }
#define M_SCL_OUT(x) { i2c_if.scl(x); mi2c_delay(); }
#define M_SDA_IN() i2c_if.sda(1)

void mi2c_start()
{
	M_SDA_OUT(0);
	M_SCL_OUT(0);
}

void mi2c_repeat_start()
{
	M_SDA_OUT(1);
	M_SCL_OUT(1);
	M_SDA_OUT(0);
	M_SCL_OUT(0);
}

void mi2c_stop()
{
	M_SDA_OUT(0);
	M_SCL_OUT(1);
	M_SDA_OUT(1);
}

unsigned char mi2c_put_byte(unsigned char data)
{
	char i;
	unsigned char ack;

	for (i = 0; i < 8; i++, data <<= 1) {
		M_SDA_OUT(data & 0x80);
		M_SCL_OUT(1);
		M_SCL_OUT(0);
	}

	M_SDA_OUT(1);
	M_SCL_OUT(1);

	ack = M_SDA_IN();	// ack: sda is pulled low ->success.

	M_SCL_OUT(0);
	M_SDA_OUT(0);

	return ack != 0;
}

void mi2c_get_byte(unsigned char *data, uint8_t last)
{
	int i;
	unsigned char indata = 0;

	M_SDA_OUT(1);
	// assert: scl is low
	M_SCL_OUT(0);

	for (i = 0; i < 8; i++) {
		M_SCL_OUT(1);
		indata <<= 1;
		if (M_SDA_IN())
			indata |= 0x01;
		M_SCL_OUT(0);
	}

	if (last) {
		M_SDA_OUT(1);	//noack
		M_SCL_OUT(1);
		M_SCL_OUT(0);
	} else {
		M_SDA_OUT(0);	//ack
		M_SCL_OUT(1);
		M_SCL_OUT(0);
	}

	*data = indata;

}

void mi2c_init()
{
	M_SCL_OUT(1);
	M_SDA_OUT(1);
}

uint8_t mi2c_devprobe(uint8_t i2c_addr)
{
	uint8_t ret;
	mi2c_start();
	ret = !mi2c_put_byte(i2c_addr << 1);
	mi2c_stop();

	return ret;
}


void mi2c_scan()
{
	int i;

	pp_printf("Scanning I2C bus %d\n", 0);
	for(i=0;i<0x80;i++)
	{
		if(mi2c_devprobe(i))
			pp_printf("Found I2C device @ %x\n", i);
	}

}
