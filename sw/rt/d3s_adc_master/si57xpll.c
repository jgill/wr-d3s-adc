/*
 * This work is part of the White Rabbit Node Core project.
 *
 * Copyright (C) 2013-2015 CERN (www.cern.ch)
 * Author: Eva Calvo <eva.calvo@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

/*.
 * WR Distributed DDS Realtime Firmware.
 *
 * si57xpll.c: RT driver for sillicon Labs Si570 Programmable oscillator.
 */

#include <stdio.h>
#include <stdint.h>

#include <libmockturtle-rt.h>

// WB interfaces
#include "hw/si570_if_wb.h"
#include "i2c.h"
#include "d3s_adc_master.h"


#define SI57X_ADDR 0x55     // Si57 I2C bus address
//Si57x WB slave core address  --> @d3s_adc.h now

static int aux_scl(int state)
{
	int value = (dp_readl(SI570_REG_GPSR + BASE_SI57X) & SI570_GPSR_SCL) ? 1 : 0;

	if(state)
		dp_writel(SI570_GPSR_SCL, SI570_REG_GPSR + BASE_SI57X);// Set SCL
	else
		dp_writel(SI570_GPCR_SCL, SI570_REG_GPCR + BASE_SI57X);// Clear SCL

	return value;
}

static int aux_sda(int state)
{
	int value = (dp_readl(SI570_REG_GPSR + BASE_SI57X) & SI570_GPSR_SDA) ? 1 : 0;

	if(state)
		dp_writel(SI570_GPSR_SDA, SI570_REG_GPSR + BASE_SI57X); // Set SDA
	else
		dp_writel(SI570_GPCR_SDA, SI570_REG_GPCR + BASE_SI57X); // Clear SDA

	return value;
}



uint8_t si57x_read_reg(uint8_t reg_addr)
{
	uint8_t rv;
	mi2c_start();
	mi2c_put_byte( SI57X_ADDR << 1);
	mi2c_put_byte( reg_addr);
	mi2c_repeat_start();
	mi2c_put_byte( (SI57X_ADDR << 1) | 1);
	mi2c_get_byte( &rv, 1);
	mi2c_stop();
	return rv;
}

void si57x_write_reg(uint8_t reg_addr, uint8_t value)
{
	mi2c_start();
	mi2c_put_byte( SI57X_ADDR << 1);
	mi2c_put_byte( reg_addr);
	mi2c_put_byte( value);
	mi2c_stop();

}

uint64_t si57x_read_rfreq()
{
	// RFREQ is 38 bits long. It is stored in registers 8,9,10,11 and 12.
	// LSB bits are in register 12.
	// MSB bits are bits 5 downto 0 of register 8.

	uint64_t rfreq = (((uint64_t) si57x_read_reg(12)) << 0)  |
		(((uint64_t) si57x_read_reg(11)) << 8)  |
		(((uint64_t) si57x_read_reg(10)) << 16) |
		(((uint64_t) si57x_read_reg(9))  << 24) |
		(((uint64_t) (si57x_read_reg(8) & 0x3f)) << 32);

	return (rfreq);
}

void si57x_read_dividers(uint8_t *n1, uint8_t *hsdiv)
{
	// HS_DIV is 3 bits long. It is stored in bits 7 downto 5 of register 7.
	// It is coded,taking values from 0 to 11.

	// N1 is 7 bits long. It is stored split between registers 7 and 8.
	// N1[1:0] is stored in register 8, bits 7 downto 6.
	// N1[6:2] is stored in register 7, bits 4 downto 0.
	// Register value is -1 the divider value.

	uint8_t r7, r8;

	r7 = si57x_read_reg(7);
	r8 = si57x_read_reg(8);
	*n1 = (((r7 & 0x1f) << 2) | ((r8 >> 6) & 0x3)) + 1;

	switch(r7 >> 5) // hsdiv
	{
	case 0:*hsdiv = 4; break;
	case 1:*hsdiv = 5; break;
	case 2:*hsdiv = 6; break;
	case 3:*hsdiv = 7; break;
	case 4:*hsdiv = 0; break;
	case 5:*hsdiv = 9; break;
	case 6:*hsdiv = 0; break;
	case 7:*hsdiv = 11; break;
	}
}

void si57x_set_hsdiv(int hsdiv)
{
	si57x_write_reg(137, 0x10); //freeze DCO
	si57x_write_reg(7, (si57x_read_reg(7) & 0x1f) | (hsdiv << 5));
	si57x_write_reg(137, 0x0); //un-freeze DCO
}

uint8_t si57x_read_n1_lsb()
{
	return si57x_read_reg(8) >> 6;
}

void si57x_set_base_rfreq(uint64_t rfreq)
{
	dp_writel( (uint32_t)(rfreq >> 32) | (si57x_read_n1_lsb() << 6), SI570_REG_RFREQH + BASE_SI57X);
	dp_writel( (uint32_t)(rfreq & 0xffffffffULL), SI570_REG_RFREQL + BASE_SI57X);
}


void si57x_set_frequency(uint64_t f_out_ref, uint64_t f_out_new )
{
	uint64_t rf, f_xtal, rfreq_new, f_dco_new;
	uint8_t n1 = 0;       // Max value of hsdiv=11 -> 4 bits, it is always an integer.
	uint8_t hsdiv = 0;    // Max value of n1=(2^7-1)=127  -> 7 bits, it is always an integer.
	uint8_t r7,r8,r9,r10,r11,r12;

	rf = si57x_read_rfreq() ;
	si57x_read_dividers(&n1, &hsdiv);
	// rf is 38 bits long. It uses Q10.28 format.
	// Values [0-1023.999999996]. Resolution:3.7e-9.
	// The max. freq. of the Si570 is 1.4GHz. This number requires 31 bits to code it.
	// Let's keep this input for the moment as an integer value 31 bits long.(Resolution 1Hz)

	f_xtal = f_out_ref * hsdiv * n1 ;  // Requires 31+4+7 = 42 bits. Q42.0
	f_xtal = (f_xtal * (1ULL<<28)) / rf;

	pp_printf("Initial Si57x parameters: n1 =  %d, hsdiv = %d, Rfreq = 0x%x%x ,F_xtal = 0x%x%x \n",
		  n1, hsdiv,
		  (unsigned int)(rf>>32), (unsigned int)(rf), (unsigned int)(f_xtal>>32), (unsigned int)f_xtal );

	// Let's find the new divisor and Rfreq values
	int hsdiv_tab[] = {4,5,6,7,9,11, 0};
	int hsdiv_reg[] = {0,1,2,3,5,7, 0};

	n1 = 1;
	while (n1 <= 128) {
		int i;
		for(i = 0; hsdiv_tab[i]; i++) {

			hsdiv = hsdiv_tab[i];

			f_dco_new = f_out_new * hsdiv * n1;  // Q42.0
			//pp_printf("i= %d, hsdiv= %d , n1= %d , f_dco_new=%08x%08x \n", i, hsdiv, n1, (uint32_t)((f_dco_new)>>32), (uint32_t)(f_dco_new));

			uint64_t Si57_FDCO_MIN = 0x121152080ULL;  // 4.85 GHz. From si57x datasheet
			uint64_t Si57_FDCO_MAX = 0x151F55580ULL;  // 5.67 GHz. From si57x datasheet
			if(f_dco_new > Si57_FDCO_MIN && f_dco_new < Si57_FDCO_MAX) {

				rfreq_new = ( f_dco_new << 28) / f_xtal; //rfreq_new = ( (f_dco_new << 12) / (f_xtal) ) << (RF_Q_MASK - 12)  ;
				pp_printf("New Si57x parameters : n1 = %d, hsdiv =  %d, Rfreq = 0x%x%x , dco = 0x%x%x \n",
					  n1, hsdiv,
					  (unsigned int) ((rfreq_new)>>32),  (unsigned int) (rfreq_new),
					  (unsigned int) ((f_dco_new)>>32),  (unsigned int) (f_dco_new) ) ;

				si57x_write_reg(137, 0x10); //freeze DCO

				n1--;
				r7  = (hsdiv_reg[i] << 5) | (n1 >> 2);
				r8  = ((n1 & 0x3) << 6) | (uint32_t)(rfreq_new >> 32);
				r9  = (rfreq_new >> 24) & 0xff;
				r10 = (rfreq_new >> 16) & 0xff;
				r11 = (rfreq_new >> 8)  & 0xff;
				r12 = (rfreq_new >> 0)  & 0xff;

				pp_printf("r7..r12 registers= %02x %02x %02x %02x %02x %02x\n", r7, r8, r9, r10, r11, r12);

				si57x_write_reg(7, r7);
				si57x_write_reg(8, r8);
				si57x_write_reg(9, r9);
				si57x_write_reg(10, r10);
				si57x_write_reg(11, r11);
				si57x_write_reg(12, r12);

				si57x_write_reg(137, 0x0); //un-freeze DCO
				si57x_write_reg(135, 1<<6); //set NewFreq bit

				si57x_set_base_rfreq(rfreq_new);  // Save RFREQ in WB registers

				// Checking that values were correctly wroten...
				uint64_t r_rfreq = si57x_read_rfreq();    //Read back for checking
				si57x_read_dividers(&n1, &hsdiv);  // Read back for checking
				pp_printf("Read dividers back:  N1 = %d, HSDIV = %d \n", n1, hsdiv );
				//pp_printf("RF set : HI=%08x LO=%08x  \n", (unsigned int)(rfreq_new>>32) , (unsigned int)(rfreq_new) );
				pp_printf("Read RFreq back : HI=%08x LO=%08x  \n", (unsigned int)(r_rfreq>>32), (unsigned int)r_rfreq  );
				return;
			}
		}

		if (n1 == 1)
			n1 = 2;
		else
			n1 += 2;
	}
	pp_printf("Failed to find an appropiate divisors and Rfreq values \n");
	return;

}


/**
 * Inits the I2C bus of the Si57x oscillator, resets the Si57x
 * and setups the Si57x registers to provide 125Mhz
 */
void si57x_init(void)
{
	mi2c_set_interface(aux_scl, aux_sda); //Configure aux_scl,Aux_sda as the func. to use
	mi2c_scan();
	si57x_write_reg(135, 0x80); // reset
	delay(300*100);  //100=1us

	si57x_set_frequency(100000000, 125000000 );
	pp_printf("Si57x programmed at 125MHz. \n");
}
