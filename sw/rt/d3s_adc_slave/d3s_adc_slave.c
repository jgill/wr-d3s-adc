/**
 * This is the real-time application to the Master
 */

#include "d3s_adc_slave.h"

#define D3S_INIT_TREV_NS 23270

struct d3s_adc_dev d3s = {
    .stream_id = 0,
    .mode = D3S_MODE_SLAVE,
    .flags = 0,
    .stream_enabled = 0,
    .err_max = 0,
    .rl_length_max = 0,
    .trans_thr_phase = 0,
    .trans_thr_count = 0,
    .last_Trev_ts_tai = 0,
    .last_Trev_ts_ns = 0,
    .last_Trev_period_ns = 0,
    .stats = {
        .slave = {
            .dec_state=7,
            .latency = 0,
            .phase_fifo_overflow = 0,
            .msg_rf_recv = 0,
            .msg_tr_recv = 0,
            .msg_invalid = 0,
        },
    },
};
struct rt_application app;

static uint32_t Trev_update =0;


/**
 * It retrieve an incoming message.
 * Today we are using the shared memory loop, it will be something else
 * in the future
 *
 */
static inline struct d3s_msg *d3s_adc_slave_message_recv(void)
{
    struct d3s_msg *msg = loop_queue_get();

    return msg;
}


/**
 * It clear the given message.
 * Today we are using the shared memory loop, it will be something else
 * in the future
 *
 */
static inline void d3s_adc_slave_message_clear(struct d3s_msg *msg)
{
    loop_queue_pop();  //update smem count and tail
}


/* This function tryes to calculate the phase correction that need to be applied at the slave node
in order to compensate for the effect of the beam time of flight.
This effect consist in the following: the beam propagation time between the RF cavities and
the destination point where it needs to be observed/measured is shorter as beam energy increases.
However, the propagation time of the timing signals through cables/fibres is constant,
and therefore the beam and the timing signals get dephased. */
static int apply_TOF(struct d3s_adc_dev *dev)
{    // FIXME: To do... TOF compensation
     //
     // Phase correction to apply to RF signal :
     //      2*PI*h*[ (frev(t-td)*td ) - (BA3_BA5_distance/SPS_circunference)]
     // Where h=4620. Or using Frf... 2*PI* [ frf(t-td)*td - h*(BA3_BA5_distance/SPS_circunference)]
     // How the gateware expects this phase correction?
    float rel_dist = 1/3; // To do: read this value from a register
    int TOF_phase = 2 * PI * HN *((dev-> last_Trev_period_ns * dev->stats.slave.latency) - rel_dist );  //in radians
    return TOF_phase;
}


/**
 * It sets the trev timestamp in the decoder
 * @param[in] seconds TAI seconds
 * @param[in] nanoseconds
 */
static inline void d3s_adc_slave_trev_ts_dec(uint32_t seconds, uint32_t nanoseconds)
{
    // Write values found to GW - DDS slave decoder
    dp_writel(seconds, BASE_D3S_ADC_SLAVE + D3SS_REG_FREV_TS_SEC);
    dp_writel(nanoseconds, BASE_D3S_ADC_SLAVE + D3SS_REG_FREV_TS_NS);
    dp_writel(D3SS_FREV_CR_VALID , BASE_D3S_ADC_SLAVE + D3SS_REG_FREV_CR);
}


/**
 * It sets the trev timestamp in the Trev generator
 * @param[in] next_ns
 * @param[in] perios_ns
 */
static inline void d3s_adc_slave_trev_ts_gen(uint32_t next_ns, uint32_t period_ns)
{
    dp_writel(next_ns, BASE_D3S_ADC_SLAVE + BASE_TrevGen + TREVGEN_REG_RM_NEXT_TICK);
    dp_writel(period_ns, BASE_D3S_ADC_SLAVE + BASE_TrevGen + TREVGEN_REG_RM_TREV);
    dp_writel(1, BASE_D3S_ADC_SLAVE + BASE_TrevGen + TREVGEN_REG_STROBE_P);
}


/**
 * It handles an incoming RF message
 * @param[in] dev D3S device
 * @parma[in] msg incoming RF message
 */
static inline void d3s_adc_slave_get_packets_rf(struct d3s_adc_dev *dev,
                                                struct d3s_msg *msg)
{
    uint32_t *records;
    int n_records, i;
    uint32_t csr;

    dev->stats.slave.msg_rf_recv++;

    n_records = msg->RF_payload.plen; //Number of records in the message
    records = msg->RF_payload.RF_data;

    d3s_adc_slave_message_clear(msg);

    for (i = 0; i < n_records; ++i) {
        csr = dp_readl(BASE_D3S_ADC_SLAVE + D3SS_REG_PHFIFO_CSR);
        if (csr & D3SS_PHFIFO_CSR_FULL) {
            dev->stats.slave.phase_fifo_overflow++;
            continue;
        }
        dp_writel(records[i], BASE_D3S_ADC_SLAVE + D3SS_REG_PHFIFO_R0);
    }
}


/**
 * It handles an incoming TRev message
 * @param[in] dev D3S device
 * @parma[in] msg incoming TRev message
 */
static inline void d3s_adc_slave_get_packets_trev(struct d3s_adc_dev *dev,
                                                  struct d3s_msg *msg)
{
    uint64_t last_Trev_ts_tai, next_Trev_ts_tai;
    uint32_t last_Trev_ts_ns, next_Trev_ts_ns, wr_tai, wr_ns;

    dev->stats.slave.msg_tr_recv++;

    // Get new data
    last_Trev_ts_tai = msg->TREV_payload.ts_tai_h;
    last_Trev_ts_tai <<= 32;
    last_Trev_ts_tai |= msg->TREV_payload.ts_tai_l;
    last_Trev_ts_ns = msg->TREV_payload.ts_ns;

    d3s_adc_slave_message_clear(msg);

    // Calculate Revolution period update
    if(Trev_update >0 ) // if this is not the first Trev tick measured
    {
        if (last_Trev_ts_tai == dev->last_Trev_ts_tai)  // in the same second than previous tick
            dev->last_Trev_period_ns = last_Trev_ts_ns - dev->last_Trev_ts_ns ;
        else
            dev->last_Trev_period_ns = WR_CYCLE_MAX_VALUE_NS + last_Trev_ts_ns - dev->last_Trev_ts_ns ;
    }


    if (dev->last_Trev_period_ns < TREV_MIN || dev->last_Trev_period_ns > TREV_MAX)
        pp_printf("Wrong Trev Period %d\n", dev->last_Trev_period_ns);

    //Update last event to calculate next frev_period
    dev->last_Trev_ts_tai = last_Trev_ts_tai;
    dev->last_Trev_ts_ns  = last_Trev_ts_ns;
    Trev_update ++ ;

    next_Trev_ts_tai = last_Trev_ts_tai;
    next_Trev_ts_ns = last_Trev_ts_ns;

    do {
        next_Trev_ts_ns += dev->last_Trev_period_ns;
        if (next_Trev_ts_ns > WR_CYCLE_MAX_VALUE_NS ) // Check if overloaded to next second
        {
            next_Trev_ts_tai++;
            next_Trev_ts_ns = next_Trev_ts_ns - WR_CYCLE_MAX_VALUE_NS;
        }

        wr_tai = lr_readl(WRN_CPU_LR_REG_TAI_SEC);
        wr_ns = (lr_readl(WRN_CPU_LR_REG_TAI_CYCLES) + REQ_TREV_PRE_WARNING + 90 ) << 3;
    } while(next_Trev_ts_tai <= wr_tai && next_Trev_ts_ns <= wr_ns);


    //Calculate the latency
    dev->stats.slave.latency = next_Trev_ts_ns - wr_ns;
    if (dev->stats.slave.latency < 0)
        dev->stats.slave.latency += WR_CYCLE_MAX_VALUE_NS;

    d3s_adc_slave_trev_ts_dec(next_Trev_ts_tai, next_Trev_ts_ns);
    d3s_adc_slave_trev_ts_gen(next_Trev_ts_ns, dev-> last_Trev_period_ns);
}


/* This function receives streamed RF data through shared memory loop back.
Use it for testing without a remote node */
static void d3s_adc_slave_get_packets(struct d3s_adc_dev *dev)
{
    struct d3s_msg *msg = d3s_adc_slave_message_recv();

    if(!msg)
        return;

    /* Dispatch messages */
    switch (msg->type) {
    case D3S_MSG_RF:
        d3s_adc_slave_get_packets_rf(dev, msg);
        break;
    case D3S_MSG_TREV_UPDATE:
        d3s_adc_slave_get_packets_trev(dev, msg);
        break;
    default:
        pp_printf("%s: Invalid message type %d", __func__, msg->type);
        dev->stats.slave.msg_invalid++;
        break;
    }
}



/**
 * Here we handle the real-time task
 */
static void d3s_adc_slave_update(struct d3s_adc_dev *dev)
{

    if (dev->stream_enabled == 0 || !wr_is_timing_ok(dev))
        return;

    d3s_adc_slave_get_packets(dev);
    dev->stats.slave.dec_state = dp_readl(BASE_D3S_ADC_SLAVE + D3SS_REG_CR) & D3SS_CR_DEC_ST_MASK;
    // FIXME: Update only when status is asked
    return;

}


/**
 * It enables/disables the sampling
 * @param[in] enable 1 to enable 0 to disable
 */
static inline void d3s_adc_slave_enable(unsigned int enable)
{
    uint32_t cr;

    cr = dp_readl(BASE_D3S_ADC_SLAVE + D3SS_REG_CR);
    if (enable)
        cr |= D3SS_CR_ENABLE;
    else
        cr &= ~D3SS_CR_ENABLE;
    dp_writel(cr, BASE_D3S_ADC_SLAVE + D3SS_REG_CR);

    d3s.stream_enabled = dp_readl(BASE_D3S_ADC_SLAVE + D3SS_REG_CR) & D3SS_CR_ENABLE;
}


/* This function intializes the vars and setups the registers
to start the allow thr slave reception of data streaming */
static int d3s_adc_slave_action_start(struct trtl_proto_header *hin, void *pin,
                                      struct trtl_proto_header *hout, void *pout)
{
    uint32_t *data = pin;

    d3s.stream_id = data[0];

    rt_send_ack(hin, pin, hout, pout); // Send ACK to host

    d3s_adc_slave_enable(0);
    dp_writel( D3SS_PHFIFO_CSR_CLEAR_BUS, BASE_D3S_ADC_SLAVE + D3SS_REG_PHFIFO_CSR); // Clears ADC slave RF fifo

    init_TOF_vars(&d3s);
    dp_writel(D3SS_DELAY_COARSE , BASE_D3S_ADC_SLAVE + D3SS_REG_REC_DELAY_COARSE);   // Setup coarse delay

    // Init msg counters (for diagnostics)
    d3s.stats.slave.msg_rf_recv = 0;
    d3s.stats.slave.msg_tr_recv = 0;
    d3s.stats.slave.msg_invalid = 0;

    loop_queue_init();          // Init Shared memory head,tail and count vars.

    Trev_update =0;

    // The first time, it's assumed to be D3S_INIT_TREV_NS
    d3s.last_Trev_period_ns = D3S_INIT_TREV_NS;
    d3s_adc_slave_enable(1);

    pp_printf("Slave streamming initialized.\n");

    return 0;
}

// This function stops the streaming of data
static int d3s_adc_slave_action_stop(struct trtl_proto_header *hin, void *pin,
                                     struct trtl_proto_header *hout, void *pout)
{
    d3s.stream_id = 0x0;
    d3s_adc_slave_enable(0);
    pp_printf("Slave stopped.\n");
    return 0;
}



#define PLL_LOCK_RETRY 1000
static int d3s_adc_slave_reset_core(void)
{
    unsigned int retry = PLL_LOCK_RETRY;
    uint32_t gpior;

    dp_writel(D3SS_RSTR_PLL_RST , BASE_D3S_ADC_SLAVE + D3SS_REG_RSTR);
    delay(1000);
    dp_writel(0, BASE_D3S_ADC_SLAVE + D3SS_REG_RSTR);
    delay(1000);

    /*
     * Wait for the SERDES PLL to lock or fail if it doesnot after
     * a PLL_LOCK_RETRY times
     */
    do {
        gpior = dp_readl(BASE_D3S_ADC_SLAVE + D3SS_REG_GPIOR);
    } while (--retry && !(gpior & D3SS_GPIOR_SERDES_PLL_LOCKED));

    delay(1000000);

    if (retry == 0) {
        pp_printf("SERDES PLL does not lock.\n");
        return -1;
    }
    return 0;
}

/* Function to initialize the HW */
static int d3s_adc_slave_init(void)
{
    int err;

    err = ad9516_init();  // VCO in Fmc-dds which generrates the 500MHz clk for the DAC
                      // and the 125 MHz WR ref clock
    if (err)
        return err;

    wr_init(&d3s);
    err = d3s_adc_slave_reset_core();
    if (err)
        return err;

    d3s_adc_slave_enable(0);

    dp_writel( D3SS_PHFIFO_CSR_CLEAR_BUS, BASE_D3S_ADC_SLAVE + D3SS_REG_PHFIFO_CSR);

    pp_printf("Slave GW initialized.\n");
    return 0;
}



static struct rt_structure d3s_in_structures[] = {
        [D3S_ADC_STRUCT_DEVICE] = {
        .struct_ptr = &d3s,
        .len = sizeof(struct d3s_adc_dev),
    },
};


static struct rt_variable d3s_in_variables[] = {

};


static action_t *d3s_in_actions[] = {
        [RT_ACTION_RECV_PING] = rt_recv_ping,
        [RT_ACTION_RECV_VERSION] = rt_version_getter,
        [RT_ACTION_RECV_FIELD_SET] = rt_variable_setter,
        [RT_ACTION_RECV_FIELD_GET] = rt_variable_getter,
        [RT_ACTION_RECV_STRUCT_SET] = rt_structure_setter,
        [RT_ACTION_RECV_STRUCT_GET] = rt_structure_getter,
//        [D3S_SLAVE_ACTION_MODE_SET] = d3s_mode_setter,
        [D3S_ADC_ACTION_START] = d3s_adc_slave_action_start,
        [D3S_ADC_ACTION_STOP] = d3s_adc_slave_action_stop,
};


struct rt_mq mq[] = {
    [D3S_SLAVE_IN_CONTROL] = {
        .index = 1,
        .flags = RT_MQ_FLAGS_LOCAL | RT_MQ_FLAGS_INPUT,
    },
};


struct rt_application app = {
        .name = "D3S ADC SLAVE",
        .version = {
        .fpga_id = D3S_APP_ID,
                .rt_id = D3S_RT_ID_SLAVE, // uint32 identifier, host library should know this number for validation
                .rt_version = RT_VERSION(0, 1),
                .git_version = GIT_VERSION
        },
        .mq = mq,
        .n_mq = 1,

        .structures = d3s_in_structures,
        .n_structures = ARRAY_SIZE(d3s_in_structures),

        .variables = d3s_in_variables,
        .n_variables = ARRAY_SIZE(d3s_in_variables),

        .actions = d3s_in_actions,
        .n_actions = ARRAY_SIZE(d3s_in_actions),
};


int main(void)
{
        int err;

        rt_init(&app);

        err = d3s_adc_slave_init();
        if (err)
            return err;

        while (1) {
                rt_mq_action_dispatch(D3S_SLAVE_IN_CONTROL);
                wr_update_link(&d3s);
                d3s_adc_slave_update(&d3s);
        }

        return 0;
}
