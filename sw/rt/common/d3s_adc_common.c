#include "d3s_adc_common.h"
#include <stdarg.h>

// This function sets or clears the 'reg' bits selected by the 'mask'
extern void gpior_set(uint32_t mask, int value, uint32_t reg)
{
    uint32_t gpior = dp_readl(reg);
    if(value)
        gpior |= mask;
    else
	gpior &= ~mask;

    dp_writel(gpior, reg);
}

// Gets the 'reg' bit selected by the mask
extern int gpior_get(uint32_t mask, uint32_t reg)
{
    uint32_t gpior = dp_readl(reg);
    return (gpior & mask) ? 1 : 0;
}

// This function initializes the master RF encoder compression parameters
extern void init_RL(struct d3s_adc_dev *dev) {
    // The error should be compatible with the Xilinx cordic v4.0 atan output
    // phase is between -1 and 1 (normalized radians) equivalent to -PI..PI.
    //dev->lt_err_max =  (1<<(9+14))  * D3S_LT_COMP_MAX_ERR / 360;
    dev-> err_max =  (1<<(9+14))  * D3S_COMP_MAX_ERR / 360;
    dev-> rl_length_max = RL_LENGTH_MAX;
    dev-> trans_thr_phase = D3S_TRANS_THR_PHASE;
    dev-> trans_thr_count = D3S_TRANS_THR_COUNT;
}

// This function initializes the Time Of Flight compesation algorithm parameters
extern void init_TOF_vars(struct d3s_adc_dev *dev) {
    dev-> last_Trev_period_ns = 0;
    dev-> last_Trev_ts_tai = 0;
    dev-> last_Trev_ts_ns = 0;
}
