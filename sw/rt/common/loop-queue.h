/*
 * This work is part of the White Rabbit Node Core project.
 *
 * Copyright (C) 2013-2014 CERN (www.cern.ch)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */


/*.
 * WR Trigger Distribution (WRTD) Firmware.
 *
 * loop-queue.h: Shared Memory-based Loopback queue.
 */

#ifndef __LOOP_QUEUE_H
#define __LOOP_QUEUE_H

#include <stdlib.h>
#include <libmockturtle-rt.h>
#include <mockturtle-rt-smem.h>
#include "d3s_adc_common.h"

#define LOOP_QUEUE_SIZE 12

void loop_queue_init();
void loop_queue_push();
void loop_queue_pop();

struct d3s_msg *loop_queue_claim();
struct d3s_msg *loop_queue_get();

#endif
