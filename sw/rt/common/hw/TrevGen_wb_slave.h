/*
  Register definitions for slave core: TrevGen

  * File           : TrevGen_wb_slave.h
  * Author         : auto-generated by wbgen2 from TrevGen_wb_slave.wb
  * Created        : Sun Feb 26 18:20:10 2017
  * Standard       : ANSI C

    THIS FILE WAS GENERATED BY wbgen2 FROM SOURCE FILE TrevGen_wb_slave.wb
    DO NOT HAND-EDIT UNLESS IT'S ABSOLUTELY NECESSARY!

*/

#ifndef __WBGEN2_REGDEFS_TREVGEN_WB_SLAVE_WB
#define __WBGEN2_REGDEFS_TREVGEN_WB_SLAVE_WB

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <inttypes.h>
#endif

#if defined( __GNUC__)
#define PACKED __attribute__ ((packed))
#else
#error "Unsupported compiler?"
#endif

#ifndef __WBGEN2_MACROS_DEFINED__
#define __WBGEN2_MACROS_DEFINED__
#define WBGEN2_GEN_MASK(offset, size) (((1<<(size))-1) << (offset))
#define WBGEN2_GEN_WRITE(value, offset, size) (((value) & ((1<<(size))-1)) << (offset))
#define WBGEN2_GEN_READ(reg, offset, size) (((reg) >> (offset)) & ((1<<(size))-1))
#define WBGEN2_SIGN_EXTEND(value, bits) (((value) & (1<<bits) ? ~((1<<(bits))-1): 0 ) | (value))
#endif


/* definitions for register: Remote next Trev tick time Stamp */

/* definitions for register: RM_Trev */

/* definitions for register: Strobe_p */

/* definitions for register: dummy */

/* definitions for field: dummy in reg: dummy */
#define TREVGEN_DUMMY_DUMMY                   WBGEN2_GEN_MASK(0, 1)
/* [0x0]: REG Remote next Trev tick time Stamp */
#define TREVGEN_REG_RM_NEXT_TICK 0x00000000
/* [0x4]: REG RM_Trev */
#define TREVGEN_REG_RM_TREV 0x00000004
/* [0x8]: REG Strobe_p */
#define TREVGEN_REG_STROBE_P 0x00000008
/* [0xc]: REG dummy */
#define TREVGEN_REG_DUMMY 0x0000000c
#endif
