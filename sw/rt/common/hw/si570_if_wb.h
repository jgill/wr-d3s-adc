/*
  Register definitions for slave core: silabs interface

  * File           : si570_if_wb.h
  * Author         : auto-generated by wbgen2 from si570_if_wb.wb
  * Created        : Wed Apr 13 15:42:16 2016
  * Standard       : ANSI C

    THIS FILE WAS GENERATED BY wbgen2 FROM SOURCE FILE si570_if_wb.wb
    DO NOT HAND-EDIT UNLESS IT'S ABSOLUTELY NECESSARY!

*/

#ifndef __WBGEN2_REGDEFS_SI570_IF_WB_WB
#define __WBGEN2_REGDEFS_SI570_IF_WB_WB

#include <inttypes.h>

#if defined( __GNUC__)
#define PACKED __attribute__ ((packed))
#else
#error "Unsupported compiler?"
#endif

#ifndef __WBGEN2_MACROS_DEFINED__
#define __WBGEN2_MACROS_DEFINED__
#define WBGEN2_GEN_MASK(offset, size) (((1<<(size))-1) << (offset))
#define WBGEN2_GEN_WRITE(value, offset, size) (((value) & ((1<<(size))-1)) << (offset))
#define WBGEN2_GEN_READ(reg, offset, size) (((reg) >> (offset)) & ((1<<(size))-1))
#define WBGEN2_SIGN_EXTEND(value, bits) (((value) & (1<<bits) ? ~((1<<(bits))-1): 0 ) | (value))
#endif


/* definitions for register: RFREQ low part */

/* definitions for register: RFREQ hi part */

/* definitions for register: GPIO Set/Readback Register */

/* definitions for field: SIlabs I2C bitbanged SCL in reg: GPIO Set/Readback Register */
#define SI570_GPSR_SCL                        WBGEN2_GEN_MASK(0, 1)

/* definitions for field: SIlabs I2C bitbanged SDA in reg: GPIO Set/Readback Register */
#define SI570_GPSR_SDA                        WBGEN2_GEN_MASK(1, 1)

/* definitions for register: GPIO Clear Register */

/* definitions for field: SILabs I2C bitbanged SCL in reg: GPIO Clear Register */
#define SI570_GPCR_SCL                        WBGEN2_GEN_MASK(0, 1)

/* definitions for field: SIlabs I2C bitbanged SDA in reg: GPIO Clear Register */
#define SI570_GPCR_SDA                        WBGEN2_GEN_MASK(1, 1)
/* [0x0]: REG RFREQ low part */
#define SI570_REG_RFREQL 0x00000000
/* [0x4]: REG RFREQ hi part */
#define SI570_REG_RFREQH 0x00000004
/* [0x8]: REG GPIO Set/Readback Register */
#define SI570_REG_GPSR 0x00000008
/* [0xc]: REG GPIO Clear Register */
#define SI570_REG_GPCR 0x0000000c
#endif
