`define ADDR_D3S_RSTR                  7'h0
`define D3S_RSTR_PLL_RST_OFFSET 0
`define D3S_RSTR_PLL_RST 32'h00000001
`define ADDR_D3S_TCR                   7'h4
`define D3S_TCR_WR_LOCK_EN_OFFSET 0
`define D3S_TCR_WR_LOCK_EN 32'h00000001
`define D3S_TCR_WR_LOCKED_OFFSET 1
`define D3S_TCR_WR_LOCKED 32'h00000002
`define D3S_TCR_WR_TIME_VALID_OFFSET 2
`define D3S_TCR_WR_TIME_VALID 32'h00000004
`define D3S_TCR_WR_LINK_OFFSET 3
`define D3S_TCR_WR_LINK 32'h00000008
`define ADDR_D3S_WR_FREQ               7'h8
`define D3S_WR_FREQ_METER_OFFSET 0
`define D3S_WR_FREQ_METER 32'hffffffff
`define ADDR_D3S_GPIOR                 7'hc
`define D3S_GPIOR_SI57X_SCL_OFFSET 0
`define D3S_GPIOR_SI57X_SCL 32'h00000001
`define D3S_GPIOR_SI57X_SDA_OFFSET 1
`define D3S_GPIOR_SI57X_SDA 32'h00000002
`define D3S_GPIOR_SPI_CS_ADC_OFFSET 2
`define D3S_GPIOR_SPI_CS_ADC 32'h00000004
`define D3S_GPIOR_SPI_SCK_OFFSET 3
`define D3S_GPIOR_SPI_SCK 32'h00000008
`define D3S_GPIOR_SPI_MOSI_OFFSET 4
`define D3S_GPIOR_SPI_MOSI 32'h00000010
`define D3S_GPIOR_SPI_MISO_OFFSET 5
`define D3S_GPIOR_SPI_MISO 32'h00000020
`define D3S_GPIOR_SERDES_PLL_LOCKED_OFFSET 6
`define D3S_GPIOR_SERDES_PLL_LOCKED 32'h00000040
`define ADDR_D3S_SSR                   7'h10
`define ADDR_D3S_CR                    7'h14
`define D3S_CR_ENABLE_OFFSET 0
`define D3S_CR_ENABLE 32'h00000001
`define D3S_CR_ENC_ST_OFFSET 1
`define D3S_CR_ENC_ST 32'h0000000e
`define ADDR_D3S_RL_ERR_MIN            7'h18
`define ADDR_D3S_RL_ERR_MAX            7'h1c
`define ADDR_D3S_RL_LENGTH_MAX         7'h20
`define ADDR_D3S_TRANSIENT_THRESHOLD_PHASE 7'h24
`define ADDR_D3S_TRANSIENT_THRESHOLD_COUNT 7'h28
`define ADDR_D3S_CNT_FIXED             7'h2c
`define ADDR_D3S_LT_CNT_RL             7'h30
`define ADDR_D3S_ST_CNT_RL             7'h34
`define ADDR_D3S_CNT_TSTAMP            7'h38
`define ADDR_D3S_ADC_R0                7'h3c
`define D3S_ADC_R0_PAYLOAD_OFFSET 0
`define D3S_ADC_R0_PAYLOAD 32'hffffffff
`define ADDR_D3S_ADC_CSR               7'h40
`define D3S_ADC_CSR_FULL_OFFSET 16
`define D3S_ADC_CSR_FULL 32'h00010000
`define D3S_ADC_CSR_EMPTY_OFFSET 17
`define D3S_ADC_CSR_EMPTY 32'h00020000
`define D3S_ADC_CSR_CLEAR_BUS_OFFSET 18
`define D3S_ADC_CSR_CLEAR_BUS 32'h00040000
`define D3S_ADC_CSR_USEDW_OFFSET 0
`define D3S_ADC_CSR_USEDW 32'h00003fff
