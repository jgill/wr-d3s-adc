#!/bin/bash

wbgen2 -V wr_d3s_adc_slave_wb.vhd -H record -p wr_d3s_adc_slave_wbgen2_pkg.vhd -K wr_d3s_adc_slave.vh -s defines -C wr_d3s_adc_slave.h -D wr_d3s_adc_slave.html wr_d3s_adc_slave.wb 

echo ""
echo "Moving WB generated files to the following locations..."
echo ""

mv -v wr_d3s_adc_slave.html ../doc/.
mv -v wr_d3s_adc_slave_wb.vhd ../.
mv -v wr_d3s_adc_slave_wbgen2_pkg.vhd ../.
mv -v wr_d3s_adc_slave.vh ../../../testbench/include/.
cp -v wr_d3s_adc_slave.h ../../../sw/include/hw/.
mv -v wr_d3s_adc_slave.h ../../../../sw/rt/common/hw/.
